(function () {
'use strict';

var Cookie = {
  get: function get(name) {
    var matches = window.parent.document.cookie.match(new RegExp('(?:^|; )' + name.replace(/([.$?*|{}()[]\\\/\+^])/g, '\\$1') + '=([^;]*)'));

    return matches ? decodeURIComponent(matches[1]) : undefined;
  },
  set: function set(name, value) {
    var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

    var expires = options.expires;
    var cookieOption = options;
    var updatedCookie = void 0;
    var d = new Date();

    if (typeof expires === 'number' && expires) {
      d.setTime(d.getTime() + expires * 1000);
      expires = d;
    } else {
      expires = new Date(d.getTime() + 14 * 24 * 60 * 60 * 1000);
    }
    if (expires && expires.toUTCString) {
      expires = expires.toUTCString();
      cookieOption.expires = expires;
    }
    var cookieValue = encodeURIComponent(value);

    updatedCookie = name + '=' + cookieValue;
    Object.keys(cookieOption).forEach(function (propName) {
      updatedCookie += '; ' + propName;
      var propValue = cookieOption[propName];

      if (propValue !== true) {
        updatedCookie += '=' + propValue;
      }
    });
    window.parent.document.cookie = updatedCookie;
  },
  delete: function _delete(name) {
    this.set(name, '', { expires: -1 });
  }
};

var isMobile = function () {
  if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)) {
    return true;
  }

  return false;
}();

var locationUrl = document.location.href;
var appOptions;

if (locationUrl.indexOf('school.mosreg') > -1) {
  appOptions = {
    authUrl: 'https://login.school.mosreg.ru/oauth2',
    grantUrl: 'https://api.school.mosreg.ru/v1/authorizations',
    scope: 'CommonInfo,Files',
    clientId: '',
    redirectUrl: window.location.href + '/?auth=true',
    provider: 'next-survey',
    api: 'https://api.school.mosreg.ru/v1/',
    isMobile: isMobile,
    userLink: 'https://school.mosreg.ru/user/user.aspx?user=',
    cdnPath: 'https://ad.csdnevnik.ru/special/staging/next/img/',
    origin: '.school.mosreg.ru',
    admins: ['1000005762061', '1000005813233', '1000005052381']
  };
} else {
  appOptions = {
    authUrl: 'https://login.dnevnik.ru/oauth2',
    grantUrl: 'https://api.dnevnik.ru/v1/authorizations',
    scope: 'CommonInfo,Files',
    clientId: 'd0287339476d4a32b0a2bbe67ca493fb',
    redirectUrl: window.location.href + '?auth=true',
    provider: 'next-survey',
    api: 'https://api.dnevnik.ru/v1/',
    apiMobile: 'https://api.dnevnik.ru/modile/v1/',
    isMobile: isMobile,
    userLink: 'https://dnevnik.ru/user/user.aspx?user=',
    cdnPath: 'https://ad.csdnevnik.ru/special/staging/next/img/',
    origin: '.dnevnik.ru',
    admins: ['1000006315838', '1000006435101', '1000004681017', '1000004934922']
  };
}

var appOptions$1 = appOptions;

var Auth = {
  token: undefined,
  auth: function auth(callback) {
    var token;

    function getToken() {
      token = /access_token=([-0-9a-zA-Z_]+)/.exec(window.location.hash) || [];

      return token[1];
    }
    token = Cookie.get(appOptions$1.provider + '_token') || getToken();
    if (undefined !== token) {
      Cookie.delete(appOptions$1.provider + '_token');
      Cookie.set(appOptions$1.provider + '_token', token);
      this.token = token;
      if (typeof callback === 'function') {
        callback();
      }
    } else {
      var error = /error=([-0-9a-zA-Z_]+)/.exec(window.location.hash) || [];

      if (undefined !== error[1]) {
        Cookie.delete(appOptions$1.provider + '_token');
        this.token = undefined;
      } else {
        window.location.href = appOptions$1.authUrl + '?response_type=token&client_id=' + appOptions$1.clientId + '\n          &scope=' + appOptions$1.scope + '&redirect_uri=' + appOptions$1.redirectUrl;
      }
    }
  }
};

/* eslint-disable */
var API = {
  init: function init(token) {
    this.token = token;
    this.model = {};
  },

  getUserInfo: function getUserInfo(callback) {
    var that = this;

    $.ajax({
      contentType: 'application/json',
      data: JSON.stringify({}),
      dataType: 'json',
      success: function success(data) {
        // console.log(data);
        if (void 0 !== data) {

          // data.personId_str = '1000001906334';

          that.model.user = data;
          if (typeof callback === 'function') {
            callback();
          }
        }
      },
      error: function error() {
        Cookie.delete(appOptions$1.provider + '_token');
        Auth.auth();
      },
      processData: false,
      type: 'GET',
      url: appOptions$1.api + 'users/me/?access_token=' + this.token
    });
  },

  clearStorage: function clearStorage() {
    var that = this;

    function deleteKeys(data) {
      for (var i = 0; i < data.Keys.length; i++) {
        that.deleteKeyFromDB(data.Keys[i].Key);
      }
    }
    that.getKeysFromDB({
      label: appOptions$1.provider + '-activity',
      pageSize: 100,
      pageNumber: 1
    }, function (data) {
      if (data.Paging.next) {
        that.getRecursivePage(data.Paging.next, function (data) {
          deleteKeys(data);
        });
      } else {
        deleteKeys(data);
      }
    });
  },

  getMainGroup: function getMainGroup(callback, id) {
    var that = this;

    id = id || this.model.user.personId_str;
    that.getEduGroup(id, function () {
      if (that.model.eduGroups.length === 0) {
        if (typeof callback === 'function') {
          callback(null);

          return;
        }
      }
      for (var i = 0; i < that.model.eduGroups.length; i++) {
        if (that.model.eduGroups[i].type === 'Group') {
          callback(that.model.eduGroups[i]);

          return;
        }
      }
      callback(null);
    });
  },

  getMarks: function getMarks(callback) {
    var that = this,
        ans = {};

    that.getMainGroup(function (group) {
      ans.group = group;
      that.getPeriods(group.id_str, function (data) {
        $.extend(ans, data);
        that.getFinalMarks(group.id_str, function (data) {
          ans.marks = data;
          callback(ans);
        });
      });
    });
  },

  getGroupFullInfo: function getGroupFullInfo(group, callback) {
    var that = this,
        ans = {};

    that.getGroupProfile(group, function (data) {
      ans.group = data;
      if (!data) {
        ans.group = {};
        ans.group.id_str = group;
      }
      that.getSchool(function (data) {
        if (data) {
          if (data.length > 0) {
            that.getSchoolProfile(data[0].id_str, function (data) {
              ans.school = data;
              callback(ans);
            });
          } else {
            callback(null);
          }
        }
      });
    });
  },

  getGroupProfile: function getGroupProfile(group, callback) {
    $.get(appOptions$1.api + 'edu-groups/' + group + '?access_token=' + this.token, {}).done(function (data) {
      // console.log(data);
      if (void 0 !== data) {
        if (typeof callback === 'function') {
          callback(data);
        }
      }
    }).fail(function () {
      if (typeof callback === 'function') {
        callback(null);
      }
    });
  },

  getMarksFromPeriod: function getMarksFromPeriod(subjects, start, end, callback) {
    var ans = [],
        iter = 0;

    if (subjects.length === 0) {
      if (typeof callback === 'function') {
        callback(null);
      }
    }
    for (var i = 0; i < subjects.length; i++) {
      $.get(appOptions$1.api + 'persons/' + API.model.user.personId_str + '/subjects/' + subjects[i] + '/marks/' + start + '/' + end + '?access_token=' + this.token, {}).done(function (data) {
        // console.log(data);
        if (void 0 !== data) {
          ans.push({
            subject: subjects[iter],
            data: data
          });
          iter++;
          if (ans.length === subjects.length) {
            if (typeof callback === 'function') {
              callback(ans);
            }
          }
        }
      }).fail(function () {
        if (typeof callback === 'function') {
          callback(null);
        }
      });
    }
  },

  getLessons: function getLessons(person, group, start, end, callback) {
    var res = [];
    var date = new Date(start).getTime();
    var endDate = new Date(end).getTime();
    var period = 2678400000;
    var that = this;

    function rec(startPeriod, endPeriod) {
      $.get(appOptions$1.api + 'edu-groups/' + group + '/lessons/' + startPeriod + '/' + endPeriod + '?access_token=' + that.token, {}).done(function (data) {
        if (void 0 !== data) {
          res = res.concat(data);
          date += period;
          if (date < endDate) {
            rec(new Date(date).toISOString(), new Date(date + period).toISOString());
          } else {
            if (typeof callback === 'function') {
              callback(res);
            }
          }
        }
      }).fail(function () {
        if (typeof callback === 'function') {
          callback(null);
        }
      });
    }
    rec(new Date(date).toISOString(), new Date(date + period).toISOString());
  },

  getAllMarks: function getAllMarks(person, school, start, end, callback) {
    $.get(appOptions$1.api + 'persons/' + person + '/schools/' + school + '/marks/' + start + '/' + end + '?access_token=' + this.token, {}).done(function (data) {
      if (void 0 !== data) {
        /* eslint-disable */
        console.log(data);
        /* eslint-enable */
      }
    }).fail(function () {
      if (typeof callback === 'function') {
        callback(null);
      }
    });
  },
  getSchedule: function getSchedule(start, end, callback) {
    //    const that = this;

    this.getUserInfo(function () {
      API.getMainGroup(function (group) {
        API.getTimeTable(group.id_str, function (timetable) {
          $.get(appOptions$1.api + 'persons/' + API.model.user.personId_str + '/groups/' + group.id_str + '/schedules?startDate=' + start + '&endDate=' + end + '&access_token=' + API.token, {}).done(function (data) {
            if (typeof callback === 'function') {
              callback({
                schedule: data,
                timetable: timetable
              });
            }
          }).fail(function () {
            if (typeof callback === 'function') {
              callback(null);
            }
          });
        });
      });
    });
  },


  getTimeTable: function getTimeTable(group, callback) {
    $.get(appOptions$1.api + 'edu-groups/' + group + '/timetables?access_token=' + this.token, {}).done(function (data) {
      // console.log(data);
      if (void 0 !== data) {
        if (typeof callback === 'function') {
          callback(data);
        }
      }
    }).fail(function () {
      if (typeof callback === 'function') {
        callback(null);
      }
    });
  },

  getSchool: function getSchool(callback, id) {
    id = id || 'me';
    $.get(appOptions$1.api + 'users/' + id + '/schools?access_token=' + this.token, {}).done(function (data) {
      // console.log(data);
      if (void 0 !== data) {
        if (typeof callback === 'function') {
          callback(data);
        }
      }
    }).fail(function () {
      if (typeof callback === 'function') {
        callback(null);
      }
    });
  },

  getSchoolProfile: function getSchoolProfile(id, callback) {
    $.get(appOptions$1.api + 'schools/' + id + '/?access_token=' + this.token, {}).done(function (data) {
      // console.log(data);
      if (void 0 !== data) {
        if (typeof callback === 'function') {
          callback(data);
        }
      }
    }).fail(function () {
      if (typeof callback === 'function') {
        callback(null);
      }
    });
  },

  getSubjects: function getSubjects(group, callback) {
    $.get(appOptions$1.api + 'edu-groups/' + group + '/subjects?access_token=' + this.token, {}, function (data) {
      // console.log(data);
      if (void 0 !== data) {
        if (typeof callback === 'function') {
          callback(data);
        }
      }
    });
  },

  getFinalMarks: function getFinalMarks(group, callback) {
    $.get(appOptions$1.api + 'persons/' + this.model.user.personId_str + '/edu-groups/' + group + '/final-marks?access_token=' + this.token, {}, function (data) {
      // console.log(data);
      if (void 0 !== data) {
        if (typeof callback === 'function') {
          callback(data);
        }
      }
    });
  },

  getPeriods: function getPeriods(group, callback) {
    $.get(appOptions$1.api + 'edu-groups/' + group + '/reporting-periods?access_token=' + this.token, {}, function (data) {
      // console.log(data);
      if (void 0 !== data) {
        var obj = {},
            current = null,
            next = null,
            dateEnd,
            i,
            now = new Date().getTime();

        for (i = 0; i < data.length; i++) {
          // dateStart = new Date(data[i].start).getTime();
          dateEnd = new Date(data[i].finish).getTime();
          if (now > dateEnd) {
            current = data[i];
          }
        }
        if (current) {
          for (i = 0; i < data.length; i++) {
            if (data[i].number === current.number + 1) {
              next = data[i];
              break;
            }
          }
        }
        obj = {
          current: current,
          next: next
        };
        if (typeof callback === 'function') {
          callback(obj);
        }
      }
    });
  },

  uploadImage: function uploadImage(fileList, callback) {
    fileList = fileList || [];
    var res = [];

    for (var i = 0; i < fileList.length; i++) {
      $.post(appOptions$1.api + 'apps/current/files/async/upload/base64/?access_token=' + this.token, {
        fileName: fileList[i].name,
        file: fileList[i].file
      }, function (data) {
        // console.log(data);
        if (void 0 !== data) {
          res.push(data);
          if (res.length === fileList.length) {
            if (typeof callback === 'function') {
              callback(res);
            }
          } else {
            if (typeof callback === 'function') {
              callback(null);
            }
          }
        }
      }).fail(function () {
        if (typeof callback === 'function') {
          callback(null);
        }
      });
    }
  },

  checkUploadImage: function checkUploadImage(fileList, callback) {
    fileList = fileList || [];
    var res = [],
        that = this,
        counter = 0,
        i = 0;

    var interval = setInterval(function () {
      $.get(appOptions$1.api + 'files/async/upload/' + fileList[i] + '/?access_token=' + that.token, {}, function (data) {
        // console.log(data);
        if (void 0 !== data) {
          res.push(data);
          i += 1;
          if (res.length === fileList.length) {
            if (typeof callback === 'function') {
              callback(res);
              clearInterval(interval);
            }
          }
        }
      });
      counter++;
      if (counter === fileList.length * 4) {
        clearInterval(interval);
      }
    }, 2000);
  },

  addKeyToDB: function addKeyToDB(options, callback) {
    options = options || {};
    $.post(appOptions$1.api + 'storage/keys/?access_token=' + this.token, {
      label: options.label,
      key: options.key,
      value: options.value,
      permissionLevel: 'Public'
    }, function (data) {
      // console.log(data);
      if (void 0 !== data) {
        if (typeof callback === 'function') {
          callback(data);
        }
      }
    });
  },

  deleteKeyFromDB: function deleteKeyFromDB(key, callback) {
    key = key || '';
    $.post(appOptions$1.api + 'storage/keys/' + key + '/delete/?access_token=' + this.token, {}, function (data) {
      // console.log(data);
      if (void 0 !== data) {
        if (typeof callback === 'function') {
          callback(data);
        }
      }
    });
  },

  getKeyFromDB: function getKeyFromDB(options, callback) {
    options = options || {};
    $.get(appOptions$1.api + 'storage/keys/' + options.key + '/?access_token=' + this.token, {}).done(function (data) {
      // console.log(data);
      if (void 0 !== data) {
        if (typeof callback === 'function') {
          callback(data);
        }
      }
    }).fail(function () {
      if (typeof callback === 'function') {
        callback(null);
      }
    });
  },

  getKeysFromDB: function getKeysFromDB(options, callback) {
    options = options || {};
    $.get(appOptions$1.api + 'storage/keys/?access_token=' + this.token, {
      label: options.label,
      page_number: options.pageNumber,
      page_size: options.pageSize,
      order_by: options.orderBy
    }, function (data) {
      // console.log(data);
      if (void 0 !== data) {
        if (typeof callback === 'function') {
          callback(data);
        }
      }
    });
  },

  getNextPage: function getNextPage(url, callback) {
    $.get(url + '&access_token=' + this.token, {}, function (data) {
      // console.log(data);
      if (void 0 !== data) {
        if (typeof callback === 'function') {
          callback(data);
        }
      }
    });
  },

  getRecursivePage: function getRecursivePage(url, callback, resData) {
    resData = resData || {
      Keys: []
    };
    var that = this;

    if (url) {
      $.get(url + '&access_token=' + this.token, {}, function (data) {
        if (void 0 !== data) {
          resData.Keys = resData.Keys.concat(data.Keys);
          that.getRecursivePage(data.Paging.next, callback, resData);
        }
      });
    } else if (typeof callback === 'function') {
      callback(resData);
    }
  },

  getFriends: function getFriends(callback) {
    var that = this;

    this.getFriendsIds(function () {
      that.getFriendsProfiles(function (data) {
        if (typeof callback === 'function') {
          callback(data);
        }
      });
    });
  },

  getFriendsIds: function getFriendsIds(callback) {
    var that = this;

    $.get(appOptions$1.api + 'users/me/friends/?access_token=' + this.token, {}, function (data) {
      // console.log(data);
      if (void 0 !== data) {
        that.model.friendsId = data;
        if (typeof callback === 'function') {
          callback();
        }
      }
    });
  },

  getChildren: function getChildren(callback) {
    var that = this;

    that.model.childrenId = [];
    $.get(appOptions$1.api + 'users/me/children/?access_token=' + this.token, {}, function (data) {
      // console.log(data);
      if (void 0 !== data) {
        that.model.childrenId = data;
        if (typeof callback === 'function') {
          callback(data);
        }
      }
    });
  },

  getProfile: function getProfile(userId, callback) {
    var that = this;

    that.model.childrenId = [];
    $.get(appOptions$1.api + 'users/' + userId + '?access_token=' + this.token, {}, function (data) {
      // console.log(data);
      if (void 0 !== data) {
        that.model.profile = data;
        if (typeof callback === 'function') {
          callback(data);
        }
      }
    });
  },

  getEduGroup: function getEduGroup(userId, callback) {
    var that = this;

    $.get(appOptions$1.api + 'persons/' + userId + '/edu-groups?access_token=' + this.token, {}, function (data) {
      // console.log(data);
      if (void 0 !== data) {
        that.model.eduGroups = data;
        if (typeof callback === 'function') {
          callback();
        }
      }
    });
  },

  getProfiles: function getProfiles(list, callback) {
    //    var that = this;

    $.ajax({
      contentType: 'application/json',
      data: JSON.stringify(list),
      dataType: 'json',
      success: function success(data) {
        // console.log(data);
        if (void 0 !== data) {
          //          that.model.friendsProfiles = data;
          if (typeof callback === 'function') {
            callback(data);
          }
        }
      },
      processData: false,
      type: 'POST',
      url: appOptions$1.api + 'users/many?access_token=' + this.token
    });
  },

  getFriendsProfiles: function getFriendsProfiles(callback) {
    var that = this;

    $.ajax({
      contentType: 'application/json',
      data: JSON.stringify(that.model.friendsId),
      dataType: 'json',
      success: function success(data) {
        // console.log(data);
        if (void 0 !== data) {
          that.model.friendsProfiles = data;
          if (typeof callback === 'function') {
            callback(data);
          }
        }
      },
      processData: false,
      type: 'POST',
      url: appOptions$1.api + 'users/many?access_token=' + this.token
    });
  },

  sendSticker: function sendSticker(options) {
    $.ajax({
      contentType: 'application/json',
      data: JSON.stringify({
        fileUrl: options.imageUrl,
        text: options.text
      }),
      dataType: 'json',
      // success: function (data) {
      //   // console.log(data);
      //   if (void 0 !== data) {
      //     if (typeof options.callback === 'function') {
      //       options.callback();
      //     }
      //   }
      // },
      complete: function complete() {
        if (typeof options.callback === 'function') {
          options.callback();
        }
      },
      processData: false,
      type: 'POST',
      url: appOptions$1.api + 'users/' + options.id + '/wallrecord?access_token=' + this.token
    });
  },

  sendMessage: function sendMessage(options, callback) {
    callback();
    $.ajax({
      contentType: 'application/json',
      data: JSON.stringify({
        from_str: options.from,
        from: options.from_str,
        to: options.to,
        to_str: options.to_str,
        body: options.body
      }),
      dataType: 'json',
      success: function success(data) {
        // console.log(data);
        if (void 0 !== data) {
          if (typeof options.callback === 'function') {
            options.callback();
          }
        }
      },
      processData: false,
      type: 'POST',
      url: appOptions$1.api + 'messages/?access_token=' + this.token
    });
  }
};

var content = {
  result: [{
    id: 1,
    text: 'Внешний вид школьной формы',
    icon: appOptions$1.cdnPath + 'icon1.png',
    answers: [{
      id: 1,
      text: 'Форма выглядит стильно и современно'
    }, {
      id: 2,
      text: 'Внешний вид не имеет значения'
    }, {
      id: 3,
      text: 'Школьная формы выглядит устаревшей'
    }]
  }, {
    id: 2,
    text: 'Долговечность',
    icon: appOptions$1.cdnPath + 'icon1.png',
    answers: [{
      id: 1,
      text: 'Формы хватает на весь учебный год'
    }, {
      id: 2,
      text: 'Хватает примерно на полгода'
    }, {
      id: 3,
      text: 'Приходится обновлять некоторые элементы формы каждые 2-3 месяца'
    }, {
      id: 4,
      text: 'Форма изнашивается каждый месяц'
    }]
  }, {
    id: 3,
    text: 'Нравится ли школьная форма вашему ребенку?',
    icon: appOptions$1.cdnPath + 'icon2.png',
    answers: [{
      id: 1,
      text: 'Носит с удовольствием'
    }, {
      id: 2,
      text: 'Относится к ней нейтрально'
    }, {
      id: 3,
      text: 'Не любит надевать школьную форму'
    }]
  }, {
    id: 4,
    text: 'Частота стирки',
    icon: appOptions$1.cdnPath + 'icon3.png',
    answers: [{
      id: 1,
      text: '2-3 раза в неделю'
    }, {
      id: 2,
      text: '1 раз в неделю'
    }, {
      id: 3,
      text: '2-3 раза в месяц'
    }, {
      id: 3,
      text: 'Раз в месяц'
    }]
  }, {
    id: 5,
    text: 'Дефекты',
    icon: appOptions$1.cdnPath + 'icon4.png',
    answers: [{
      id: 1,
      text: 'Вытягиваются коленки или локти'
    }, {
      id: 2,
      text: 'Ткань протирается в некоторых местах'
    }, {
      id: 3,
      text: 'Ткань скатывается'
    }, {
      id: 4,
      text: 'Форма выцветает'
    }, {
      id: 5,
      text: 'Одежда растягивается/садится при стирке'
    }]
  }, {
    id: 6,
    text: 'Способ стирки школьной формы',
    icon: appOptions$1.cdnPath + 'icon5.png',
    answers: [{
      id: 1,
      text: 'Вручную'
    }, {
      id: 2,
      text: 'В стиральной машине'
    }]
  }, {
    id: 7,
    text: 'Стирка и качество школьной формы',
    icon: appOptions$1.cdnPath + 'icon6.png',
    answers: [{
      id: 1,
      text: 'Форма изнашивается после каждой стирки'
    }, {
      id: 2,
      text: 'Форма изнашивается после нескольких стирок'
    }, {
      id: 3,
      text: 'Почти никак не отражается на качестве формы'
    }]
  }, {
    id: 8,
    text: 'Глажка школьной формы',
    icon: appOptions$1.cdnPath + 'icon7.png',
    answers: [{
      id: 1,
      text: 'Гладится быстро и легко'
    }, {
      id: 2,
      text: 'Разглаживается с трудом'
    }]
  }, {
    id: 9,
    text: 'Самые важные характеристики школьной формы по мнению родителей',
    icon: appOptions$1.cdnPath + 'icon8.png',
    answers: [{
      id: 1,
      text: 'Модные и стильные образы'
    }, {
      id: 2,
      text: 'Удобство и комфорт'
    }, {
      id: 3,
      text: 'Широкая цветовая гамма'
    }, {
      id: 4,
      text: 'Качественные натуральные ткани'
    }, {
      id: 5,
      text: 'Удобная стирка и глажка'
    }]
  }],
  test: [{
    id: 1,
    text: 'Удовлетворены ли вы внешним видом школьной формы ребенка?',
    answers: [{
      id: 1,
      text: 'Да, форма выглядит стильно и современно'
    }, {
      id: 2,
      text: 'Внешний вид не имеет значения'
    }, {
      id: 3,
      text: 'Нет, школьная формы выглядит устаревшей'
    }]
  }, {
    id: 2,
    text: 'Надолго ли хватает одного комплекта школьной формы?',
    answers: [{
      id: 1,
      text: 'Да, хватает на весь учебный год'
    }, {
      id: 2,
      text: 'Хватает примерно на полгода'
    }, {
      id: 3,
      text: 'Приходится обновлять некоторые элементы формы каждые 2-3 месяца'
    }, {
      id: 4,
      text: 'Форма изнашивается каждый месяц'
    }]
  }, {
    id: 3,
    text: 'Нравится ли школьная форма вашему ребенку?',
    answers: [{
      id: 1,
      text: 'Да, носит с удовольствием'
    }, {
      id: 2,
      text: 'Относится к ней нейтрально'
    }, {
      id: 3,
      text: 'Нет, не любит надевать школьную форму'
    }]
  }, {
    id: 4,
    text: 'Как часто вы стираете школьную форму ребенка?',
    answers: [{
      id: 1,
      text: '2-3 раза в неделю'
    }, {
      id: 2,
      text: '1 раз в неделю'
    }, {
      id: 3,
      text: '2-3 раза в месяц'
    }, {
      id: 4,
      text: 'Раз в месяц'
    }]
  }, {
    id: 5,
    text: 'Какие дефекты формы проявляются при носке? (можно выбрать несколько вариантов ответа)',
    multi: 1,
    answers: [{
      id: 1,
      text: 'Вытягиваются коленки или локти'
    }, {
      id: 2,
      text: 'Ткань протирается в некоторых местах'
    }, {
      id: 3,
      text: 'Ткань скатывается'
    }, {
      id: 4,
      text: 'Форма выцветает'
    }, {
      id: 5,
      text: 'Одежда растягивается/садится при стирке'
    }]
  }, {
    id: 6,
    text: 'Каким способом вы стираете школьную форму?',
    answers: [{
      id: 1,
      text: 'Вручную'
    }, {
      id: 2,
      text: 'В стиральной машине'
    }]
  }, {
    id: 7,
    text: 'Как стирка отражается на качестве школьной формы?',
    answers: [{
      id: 1,
      text: 'Форма изнашивается после каждой стирки'
    }, {
      id: 2,
      text: 'Форма изнашивается после нескольких стирок'
    }, {
      id: 3,
      text: 'Почти никак не отражается на качестве формы'
    }]
  }, {
    id: 8,
    text: 'Легко ли гладить школьную форму?',
    answers: [{
      id: 1,
      text: 'Да, форма гладится быстро и легко'
    }, {
      id: 2,
      text: 'Форма разглаживается с трудом'
    }]
  }, {
    id: 9,
    text: 'Какие характеристики школьной формы вы считаете наиболее важными? (можно выбрать несколько вариантов ответа)',
    multi: 1,
    answers: [{
      id: 1,
      text: 'Модные и стильные образы'
    }, {
      id: 2,
      text: 'Удобство и комфорт'
    }, {
      id: 3,
      text: 'Широкая цветовая гамма'
    }, {
      id: 4,
      text: 'Качественные натуральные ткани'
    }, {
      id: 5,
      text: 'Удобная стирка и глажка'
      // {
      //   id: 6,
      //   text: 'Свой вариант'
      // }
    }]
  }]
};

var Loader = {
  show: function show() {
    var html = '<div class="loader"><img class="loader__img" src="' + appOptions$1.cdnPath + 'loader.gif"/></div>';

    $('body').append(html);
  },

  hide: function hide() {
    $('.loader').remove();
  }
};

var Modal = {
  overflow: '',
  open: function open(content, callback, noClose) {
    var html = '',
        that = this;

    html += '<div class="overlay">';
    html += '<div class="modal';
    html += noClose ? ' modal-video' : '';
    html += '">';
    html += '<div class="modal-content';
    html += noClose ? ' modal-content-video' : '';
    html += '">';
    if (noClose) {
      html += '<img class="modal-close" src="' + appOptions$1.cdnPath + 'close.png' + '">';
    }
    html += content + '</div>';
    html += '<div>';
    html += '</div>';
    this.close();
    $('body').append(html);
    that.overflow = $('body').css('overflow');
    $('body').css('overflow', 'hidden');
    this.setCenter();
    $(window).on('resize', this.setCenter);

    if (typeof callback === 'function') {
      callback();
    }

    $('.modal-close').on('click', function () {
      that.close();
    });
  },

  setCenter: function setCenter() {
    var h = $('.modal').innerHeight(),
        w = $('.modal').innerWidth();

    $('.modal-').css('margin', '-' + Math.floor(h / 2) + 'px ' + '0 0 -' + Math.floor(w / 2) + 'px');
  },

  close: function close() {
    $('.overlay').remove();
    $('body').css('overflow', this.overflow);
  }
};

var Activity = {
  model: {},

  get: function get(callback) {
    var that = this;

    API.getKeyFromDB({ key: appOptions$1.provider + '-activity-user_' + API.model.user.id_str }, function (data) {
      // eslint-disable-next-line
      // data = null;
      if (data) {
        var json = JSON.parse(data.Value);

        that.model = json;
      } else {
        that.model = {
          result: [],
          photo: [],
          user: API.model.user
        };
        that.set();
      }
      if (typeof callback === 'function') {
        callback(data === null);
      }
    });
  },
  set: function set(callback) {
    var that = this;

    API.addKeyToDB({
      label: appOptions$1.provider + '-activity',
      key: appOptions$1.provider + '-activity-user_' + API.model.user.id_str,
      value: JSON.stringify(that.model)
    }, function () {
      if (typeof callback === 'function') {
        callback();
      }
    });
  }
};

var Result = {
  model: {},

  get: function get(callback) {
    var that = this;

    API.getKeyFromDB({ key: appOptions$1.provider + '-survey-result' }, function (data) {
      // eslint-disable-next-line
      // data = null;
      if (data) {
        var json = JSON.parse(data.Value);

        that.model = json;
      } else {
        that.model = {
          1: {
            1: 0,
            2: 0,
            3: 0
          },
          2: {
            1: 0,
            2: 0,
            3: 0,
            4: 0
          },
          3: {
            1: 0,
            2: 0,
            3: 0
          },
          4: {
            1: 0,
            2: 0,
            3: 0,
            4: 0
          },
          5: {
            1: 0,
            2: 0,
            3: 0,
            4: 0,
            5: 0
          },
          6: {
            1: 0,
            2: 0
          },
          7: {
            1: 0,
            2: 0,
            3: 0
          },
          8: {
            1: 0,
            2: 0
          },
          9: {
            1: 0,
            2: 0,
            3: 0,
            4: 0,
            5: 0,
            6: 0
          }
        };
        that.set();
      }
      if (typeof callback === 'function') {
        callback(data === null);
      }
    });
  },
  set: function set(callback) {
    var that = this;

    API.addKeyToDB({
      label: appOptions$1.provider + '-survey-result',
      key: appOptions$1.provider + '-survey-result',
      value: JSON.stringify(that.model)
    }, function () {
      if (typeof callback === 'function') {
        callback();
      }
    });
  }
};

var wordForm = {
  get: function get(key, number) {
    var res, tail;

    tail = number;
    while (tail > 20) {
      tail -= 20;
    }
    switch (key) {
      case 'score':
        if (number == 1 || tail === 1 || tail === 11) {
          res = 'балл';
        } else if (number >= 5 && number <= 20 || tail >= 5 && tail <= 20 || tail === 0) {
          res = 'баллов';
        } else if (tail == 2 || tail == 3 || tail == 4) {
          res = 'балла';
        }
        break;
      case 'day':
        if (number == 1 || tail === 1 || tail === 11) {
          res = 'день';
        } else if (number >= 5 && number <= 20 || tail >= 5 && tail <= 20 || tail === 0) {
          res = 'дней';
        } else if (tail == 2 || tail == 3 || tail == 4) {
          res = 'дня';
        }
        break;
      case 'friend':
        if (number == 1 || tail === 1 || tail === 11) {
          res = 'друга';
        } else if (number >= 5 && number <= 20 || tail >= 5 && tail <= 20 || tail === 0) {
          res = 'друзей';
        } else if (tail == 2 || tail == 3 || tail == 4) {
          res = 'друга';
        }
        break;
      case 'second':
        if (number == 1 || tail === 1 || tail === 11) {
          res = 'секунда';
        } else if (number >= 5 && number <= 20 || tail >= 5 && tail <= 20 || tail === 0) {
          res = 'секунд';
        } else if (tail == 2 || tail == 3 || tail == 4) {
          res = 'секунды';
        }
        break;
      case 'lesson':
        if (number == 1 || tail === 1 || tail === 11) {
          res = 'урок';
        } else if (number >= 5 && number <= 20 || tail >= 5 && tail <= 20 || tail === 0) {
          res = 'уроков';
        } else if (tail == 2 || tail == 3 || tail == 4) {
          res = 'урока';
        }
        break;
      case 'hour':
        if (number == 1 || tail === 1 || tail === 11) {
          res = 'час';
        } else if (number >= 5 && number <= 20 || tail >= 5 && tail <= 20 || tail === 0) {
          res = 'часов';
        } else if (tail == 2 || tail == 3 || tail == 4) {
          res = 'часа';
        }
        break;
      case 'age':
        if (number == 1 || tail === 1) {
          res = 'год';
        } else if (number >= 5 && number <= 20 || tail >= 5 && tail <= 20 || tail === 0) {
          res = 'лет';
        }
        if (tail == 2 || tail == 3 || tail == 4 || number === 33) {
          res = 'года';
        }
        break;
      case 'participant':
        if (number == 1 || tail === 1) {
          res = 'участник';
        } else if (number >= 5 && number <= 20 || tail >= 5 && tail <= 20 || tail === 0) {
          res = 'участников';
        }
        if (tail == 2 || tail == 3 || tail == 4 || number === 33) {
          res = 'участника';
        }
        break;
    }

    return res;
  }
};

var dateTime = {
  get: function get(time) {
    var d = new Date(time);
    var month = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
    var today = new Date();

    today.setHours(0);
    if (d.getTime() > today && d.getTime() < today + 24 * 60 * 60 * 1000) {
      return 'Сегодня';
    }
    var day = d.getDate() < 10 ? '0' + d.getDate() : d.getDate();

    return day + '.' + month[d.getMonth()] + '.' + d.getFullYear();
  }
};

var toConsumableArray = function (arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  } else {
    return Array.from(arr);
  }
};

var App = {
  totalCount: 0,
  currentCount: 0,
  overflow: '',
  ext: '',
  imageWidth: 0,
  imageHeight: 0,
  base64: [],
  photos: [],

  init: function init() {
    var that = this;

    API.init();

    $('.photo-miniature__img').on('click', function () {
      $('.photo-large__img').prop('src', $(this).prop('src'));
      $('.photo-large__link').prop('href', $(this).data('href'));
    });

    $(window).on('resize scroll', function () {
      if (!appOptions$1.isMobile) {
        return;
      }
      var scroll = Math.floor($(document).height() / $(window).scrollTop());

      $('.col').removeClass('col--active');
      if (scroll < 5) {
        $('.col--r').addClass('col--active');
      } else if (scroll < 20) {
        $('.col--c').addClass('col--active');
      } else {
        $('.col--l').addClass('col--active');
      }
    });

    $('.contest-desc__road-map-item').hover(function () {
      var id = $(this).data('id');

      $('.contest-desc__road-map-item--start').removeClass('contest-desc__road-map-item--start');
      $('.contest-desc__msg-mob--start').removeClass('contest-desc__msg-mob--start');
      $('.contest-desc__msg-mob[data-id=' + id + ']').css('opacity', '1');
    }, function () {
      var id = $(this).data('id');

      $('.contest-desc__msg-mob[data-id=' + id + ']').css('opacity', '0');
    });

    $('.nav-mobile').on('click', function () {
      $('.survey-bg__item-text[data-id="1"]').click();
    });

    $('.next-btn, .col, .col__content').on('click', function (e) {
      // e.preventDefault();
      e.stopPropagation();
      var id = $(this).data('id');

      if ($(this).hasClass('col--large')) {
        return;
      }

      switch (id) {
        case 1:
          if ($(this).hasClass('next-btn') || appOptions$1.isMobile && $(this).hasClass('col__content')) {
            window.open('https://www.next.com.ru/ru/back-to-school');
          } else {
            $('.survey-bg__item-text').addClass('survey-bg__item-text--hide').removeClass('survey-bg__item-text--show');
            $('.col').removeClass('col--large col--page col--c-large col--r-large');
            $('.nav-mobile').removeClass('nav-mobile-show');
            $('.col__content').fadeIn();

            $('.header').show();
            $('.survey-container').hide();
            $('.contest-page').hide();
          }
          break;
        case 2:
          $('.col__content').hide();
          $('.contest-page').hide();
          $('.header').hide();

          $('.nav-mobile').addClass('nav-mobile-show');

          $('.col').removeClass('col--large col--r-large').addClass('col--page');
          $('.col--c').addClass('col--large col--c-large');

          $('.col--l').children('.survey-bg__item-text').removeClass('survey-bg__item-text--hide').addClass('survey-bg__item-text--show');
          $('.col--r').children('.survey-bg__item-text').removeClass('survey-bg__item-text--hide').addClass('survey-bg__item-text--show');
          $('.col--c').children('.survey-bg__item-text').addClass('survey-bg__item-text--hide').addClass('survey-bg__item-text--show');

          $('.survey-container').fadeIn();
          that.checkSession();
          break;
        case 3:
          $('.col__content').hide();
          $('.survey-container').hide();
          $('.header').hide();

          $('.nav-mobile').addClass('nav-mobile-show');

          $('.col').removeClass('col--large col--c-large').addClass('col--page');
          $('.col--r').addClass('col--large col--r-large');

          $('.col--l').children('.survey-bg__item-text').removeClass('survey-bg__item-text--hide').addClass('survey-bg__item-text--show');
          $('.col--c').children('.survey-bg__item-text').removeClass('survey-bg__item-text--hide').addClass('survey-bg__item-text--show');
          $('.col--r').children('.survey-bg__item-text').addClass('survey-bg__item-text--hide').addClass('survey-bg__item-text--show');

          $('.contest-page').fadeIn();
          that.checkSession();
          break;
      }
    });

    $('.send-photo').on('click', function () {
      $('.loader-layout').css('display', 'flex');
      that.overflow = $('body').css('overflow');
      $('body').css('overflow', 'hidden');
    });

    $('.modal__close').on('click', function () {
      $(this).parents('.loader-layout').css('display', 'none');
      $('body').css('overflow', that.overflow);
    });

    $('.photo__placeholder').on('click', function () {
      $('.hidden-form')[0].reset();
      $('.photo-loader').click();
    });

    $('.photo-loader').on('change', function (e) {
      e.preventDefault();
      if (!e.target.files.length) {
        return;
      }
      Loader.show();
      var reader = new FileReader();

      var fileName = e.target.files[0].name.split('.');

      that.ext = fileName[fileName.length - 1];
      reader.onload = function (event) {
        var img = new Image();

        img.onload = function loader() {
          Loader.hide();
          var ratioX = 110 / this.width;
          var ratioY = 80 / this.height;
          var ratio = ratioX < ratioY ? ratioX : ratioY;

          that.imageWidth = parseInt(this.width * ratio, 10);
          that.imageHeight = parseInt(this.height * ratio, 10);
          that.base64.push(event.target.result);
          if (that.base64[that.base64.length - 1].length > 6000000) {
            $('.warn').html('\n          \u0418\u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u0438\u0435 \u0441\u043B\u0438\u0448\u043A\u043E\u043C \u0431\u043E\u043B\u044C\u0448\u043E\u0435.<br>\n          \u0417\u0430\u0433\u0440\u0443\u0437\u0438\u0442\u0435 \u0438\u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u0438\u0435 \u043C\u0435\u043D\u044C\u0448\u0435\u0433\u043E \u0440\u0430\u0437\u043C\u0435\u0440\u0430 \u0432 \u0444\u043E\u0440\u043C\u0430\u0442\u0435 png, jpg \u0438\u043B\u0438 gif');
            that.base64 = [];
          } else {
            var html = '\n          <img src="' + that.base64[that.base64.length - 1] + '" width="' + that.imageWidth + '" height="' + that.imageHeight + '" class="loaded-photo">\n          <span class="remove-photo"></span>';

            $('.photo__placeholder').html(html);
          }
        };
        img.src = URL.createObjectURL(e.target.files[0]);
      };
      reader.readAsDataURL(e.target.files[0]);
    });

    $('.photo__placeholder').on('click', '.remove-photo', function (e) {
      e.stopPropagation();
      $(this).prev('.loaded-photo').remove();
      $(this).remove();
    });

    $('.load-photo').on('click', function () {
      if (that.base64.length === 0) {
        return;
      }
      $('.warn').empty();
      Loader.show();
      var files = [];

      that.base64.forEach(function (item) {
        return files.push({
          name: new Date().getTime() + '.' + that.ext,
          file: item.split('base64,')[1]
        });
      });
      that.ext = '';
      that.base64 = [];
      API.uploadImage(files, function (res) {
        if (res) {
          API.checkUploadImage(res, function (uploadFiles) {
            var _Activity$model$photo;

            (_Activity$model$photo = Activity.model.photo).push.apply(_Activity$model$photo, toConsumableArray(uploadFiles.map(function (item) {
              return item.downloadUrl;
            })));
            API.addKeyToDB({
              label: appOptions$1.provider + '-gallery',
              key: appOptions$1.provider + '-photo_' + API.model.user.id_str + '-' + new Date().getTime(),
              value: JSON.stringify({ user: API.model.user, photo: uploadFiles[0].downloadUrl })
            }, function () {
              Loader.hide();
            });
            Activity.set();
            if (uploadFiles) {
              $('.warn').html('Изображение успешно загружено');
              $('.photo__placeholder').empty();
              $('.modal__close').click();
              that.getGallery();
            } else {
              $('.warn').html('Произошла ошибка при загрузке изображения.');
            }
          });
        } else {
          Loader.hide();
          $('.warn').html('Произошла ошибка при загрузке изображения.');
        }
      });
    });
  },

  checkSession: function checkSession() {
    var that = this;

    if (document.location.href.indexOf('access_token') > -1) {
      Auth.auth(function () {
        var path = window.location.pathname.substring(0, window.location.pathname.length);

        history.pushState('', document.title, path);
        API.token = Cookie.get(appOptions$1.provider + '_token');
        check();
      });
    } else if (typeof Cookie.get(appOptions$1.provider + '_token') !== 'undefined') {
      API.token = Cookie.get(appOptions$1.provider + '_token');
      check();
    } else {
      var html = '\n        <div class="auth-title">\u0410\u0432\u0442\u043E\u0440\u0438\u0437\u0430\u0446\u0438\u044F</div>\n        <div class="auth-text">\u0414\u043B\u044F \u043F\u0440\u043E\u0434\u043E\u043B\u0436\u0435\u043D\u0438\u044F \u043D\u0435\u043E\u0431\u0445\u043E\u0434\u0438\u043C\u043E \u0430\u0432\u0442\u043E\u0440\u0438\u0437\u043E\u0432\u0430\u0442\u044C\u0441\u044F</div>\n        <button class="res-btn auth-btn">\u0430\u0432\u0442\u043E\u0440\u0438\u0437\u0430\u0446\u0438\u044F</button>\n      ';

      Modal.open(html);
      $('.auth-btn').on('click', function () {
        Auth.auth();
      });
    }

    function check() {
      Loader.show();
      API.getUserInfo(function () {
        Activity.get(function () {
          Loader.hide();
          if ($('.col--large').data('id') === 2) {
            if (Activity.model.result.length) {
              // that.drawSurvey()
              that.drawResults();
            } else {
              that.drawSurvey();
            }
          } else {
            that.getGallery();
          }
        });
      });
    }
  },

  getGallery: function getGallery(step) {
    var that = this;

    Loader.show();
    API.getKeysFromDB({
      label: appOptions$1.provider + '-gallery',
      pageSize: 50,
      pageNumber: step || 1,
      orderBy: 'date_desc'
    }, function (data) {
      Loader.hide();
      that.drawGallery(data, step);
    });
  },

  drawGallery: function drawGallery(data, step) {
    var that = this;
    var newData = data.Keys.map(function (item) {
      return {
        key: item.Key,
        date: item.CreatedDate,
        data: JSON.parse(item.Value)
      };
    });
    var html = '';

    that.photos = [].concat(toConsumableArray(that.photos), toConsumableArray(newData));

    if (typeof step === 'undefined') {
      this.totalCount = data.Paging.count;
      html += '\n    <div class="gallery-container">\n      <div class="gallery__title">\n        \u0423\u0436\u0435 <span class="count">' + this.totalCount + '</span> ' + wordForm.get('participant', this.totalCount) + '\n      </div>\n      <div class="gallery-content">\n    ';
    }
    this.currentCount = that.photos.length;

    for (var i = (step - 1 || 0) * 50; i < that.photos.length; i++) {
      html += '\n      <div class="gallery__item" data-id="' + that.photos[i].key + '">\n        ' + (appOptions$1.admins.includes(API.model.user.id_str) ? '<div class="gallery__item-remove" data-id="' + that.photos[i].key + '">x</div>' : '') + '\n        \n        <img class="gallery__item-photo" src="' + that.photos[i].data.photo + '">\n      </div>\n    ';
    }

    if (typeof step === 'undefined') {
      html += '</div>';
      if (this.totalCount > this.currentCount) {
        html += '<button class="res-btn next-step" data-id="2">Загрузить еще</button>';
      }
      html += '</div>';
    } else if (this.totalCount > this.currentCount) {
      html += '<button class="res-btn next-step" data-id="' + (step + 1) + '">\u0417\u0430\u0433\u0440\u0443\u0437\u0438\u0442\u044C \u0435\u0449\u0435</button>';
    }

    if (typeof step === 'undefined') {
      $('.gallery-container').remove();
      $('.contest-page').append(html);
    } else {
      $('.gallery-content').append(html);
    }

    $('.gallery__item').on('click', function () {
      var _this = this;

      var img = new Image();
      var data = that.photos.find(function (item) {
        return item.key === $(_this).data('id');
      });

      img.onload = function loader() {
        var imgW = this.width;
        var imgH = this.height;
        var ratio = this.width / this.height;

        ratio = ratio > 1 ? ratio : this.height / this.width;
        if (this.width >= this.height) {
          imgW = this.width > document.documentElement.clientWidth * 0.7 ? document.documentElement.clientWidth * 0.7 : this.width;
          imgH = imgW / ratio;
          if (imgH > document.body.clientHeight * 0.7) {
            imgH = document.body.clientHeight * 0.7;
            imgW = imgH * ratio;
          }
        } else {
          imgH = this.height > document.body.clientHeight * 0.7 ? document.body.clientHeight * 0.7 : this.height;
          imgW = imgH / ratio;
        }
        var mLeft = parseInt(imgW / 2, 10);
        var mTop = parseInt(imgH / 2, 10);
        var closeLeft = imgW / 2 + 20;

        var html = '\n        <div class="view-layout">\n          <div class="close-viewer" style="margin: -' + mTop + 'px 0 0 ' + closeLeft + 'px"></div>\n          <div class="user-name" style="margin: ' + (mTop + 10) + 'px 0 0 -' + mLeft + 'px" >\n            <a href="' + appOptions$1.userLink + data.data.user.id_str + '" class="user-name__link" target="_blank">\n              ' + data.data.user.firstName + ' ' + data.data.user.lastName + '\n            </a>\n            <div class="user-name__date">\u0414\u0430\u0442\u0430 \u0434\u043E\u0431\u0430\u0432\u043B\u0435\u043D\u0438\u044F: ' + dateTime.get(data.date) + '</div>\n          </div>\n          <img src="' + this.src + '" alt="" style="margin: -' + mTop + 'px 0 0 -' + mLeft + 'px" width="' + imgW + '" height="' + imgH + '" class="view-photo">\n        </div>\n      ';

        $('body').append(html);
        that.overflow = $('body').css('overflow');
        $('body').css('overflow', 'hidden');

        $('.close-viewer').on('click', function () {
          $('.view-layout').remove();
          $('body').css('overflow', that.overflow);
        });
      };
      img.src = $(this).children('.gallery__item-photo').prop('src');
    });

    $('.gallery__item-remove').on('click', function (e) {
      e.stopPropagation();
      Loader.show();
      API.deleteKeyFromDB($(this).data('id'), function () {
        Loader.hide();
        that.getGallery();
      });
    });

    $('.contest-page').on('click', '.next-step', function () {
      that.getGallery($(this).data('id'));
      $(this).remove();
    });
  },

  drawSurvey: function drawSurvey() {
    var that = this;

    $('.survey-title__text').html('Довольны ли вы качеством школьной формы?');
    $('.survey-title__subtitle').html('Оцените форму, которую носит ваш ребенок');
    var html = '';

    for (var i = 0; i < content.test.length; i++) {
      html += '\n      <div class="survey-item" data-q="' + content.test[i].id + '">\n        <div class="survey-item__question">\n          ' + content.test[i].id + '. ' + content.test[i].text + '\n        </div>\n    ';
      for (var j = 0; j < content.test[i].answers.length; j++) {
        html += '\n        <div \n          class="survey-item__answer\n          ' + (content.test[i].multi ? ' survey-item__answer--check' : '') + '" \n          data-id="' + content.test[i].answers[j].id + '"\n        >\n          ' + content.test[i].answers[j].text + '\n        </div>\n      ';
      }
      html += '</div>';
    }
    html += '\n    <div class="error-container">\n      \u0414\u043B\u044F \u043F\u0440\u043E\u0434\u043E\u043B\u0436\u0435\u043D\u0438\u044F \u043D\u0435\u043E\u0431\u0445\u043E\u0434\u0438\u043C\u043E \u043E\u0442\u0432\u0435\u0442\u0438\u0442\u044C \u043D\u0430 \u0432\u0441\u0435 \u0432\u043E\u043F\u0440\u043E\u0441\u044B\n    </div>\n    <button class="res-btn send-survey" onclick="gtag(\'event\', \'\u041E\u043F\u0440\u043E\u0441 \u0423\u0437\u043D\u0430\u0442\u044C \u0440\u0435\u0437\u0443\u043B\u044C\u0442\u0430\u0442 \u041A\u043B\u0438\u043A\', { \'event_category\': \'\u041E\u043F\u0440\u043E\u0441 \u0423\u0437\u043D\u0430\u0442\u044C \u0440\u0435\u0437\u0443\u043B\u044C\u0442\u0430\u0442\', \'event_action\':\n              \'\u041A\u043B\u0438\u043A\', });">\u0443\u0437\u043D\u0430\u0442\u044C \u0440\u0435\u0437\u0443\u043B\u044C\u0442\u0430\u0442</button>\n  ';
    $('.survey-content').html(html);
    $('.survey-container').show();

    $('.survey-item__answer').on('click', function () {
      var parent = $(this).parents('.survey-item');

      if ($(this).hasClass('survey-item__answer--check')) {
        $(this).toggleClass('survey-item__answer--active');
      } else {
        parent.children('.survey-item__answer').removeClass('survey-item__answer--active');
        $(this).addClass('survey-item__answer--active');
      }
    });

    $('.send-survey').on('click', function () {
      var res = [];
      var error = false;

      $('.survey-item').each(function () {
        var ans = [];

        $(this).children('.survey-item__answer--active').each(function () {
          ans.push($(this).data('id'));
        });
        if (ans.length) {
          res.push({ id: $(this).data('q'), answer: ans });
        } else {
          error = true;
        }
      });
      if (error) {
        $('.error-container').show();
      } else {
        $('.error-container').hide();
        Result.get(function () {
          for (var _i = 0; _i < res.length; _i++) {
            for (var _j = 0; _j < res[_i].answer.length; _j++) {
              Result.model[res[_i].id][res[_i].answer[_j]] += 1;
            }
          }
          Loader.show();
          Result.set(function () {
            Activity.model.result = res;
            Activity.set(function () {
              Loader.hide();
              that.drawResults();
            });
          });
        });
      }
    });
  },

  drawResults: function drawResults() {
    Loader.show();
    Result.get(function () {
      Loader.hide();
      $('.survey-container').hide();
      var html = '';

      $('.survey-title__text').html('Вот что думают родители школьников о качестве школьной формы');
      $('.survey-title__subtitle').hide();

      var _loop = function _loop(i) {
        if (!Result.model.hasOwnProperty(i)) {
          return 'continue';
        }
        var total = 0;

        for (var k in Result.model[i]) {
          if (!Result.model[i].hasOwnProperty(k)) {
            continue;
          }
          total += Result.model[i][k];
        }
        var item = content.result.find(function (item) {
          return item.id === parseInt(i, 10);
        });

        html += '\n        <div class="result-item">\n          <div class="result-item__header">\n            <img class="result-item__header-icon" src="' + item.icon + '"/>\n            <div class="result-item__header-text"> ' + item.text + ' </div>\n          </div>\n        ';
        for (var j = 0; j < item.answers.length; j++) {
          var width = Result.model[i][item.answers[j].id] / total * 100;

          html += '\n          <div class="result-item__answer">\n            <div class="result-item__answer-text"> ' + item.answers[j].text + ' </div>\n            <div class="result-item__progress">';
          html += width > 0 ? '<div class="result-item__progress-line" style="width: ' + width + '%">\n          <div class="result-item__progress-text"> ' + parseInt(width, 10) + '% </div>\n        </div>' : '';
          html += '</div>\n          </div>\n        ';
        }
        html += '</div>';
      };

      for (var i in Result.model) {
        var _ret = _loop(i);

        if (_ret === 'continue') continue;
      }
      html += '\n      <div class="survey-footer">\n        <div class="survey-footer__col">\n          <div class="survey-footer__title">\n            \u041F\u043E\u0447\u0435\u043C\u0443 NEXT?\n          </div> \n          <ul class="survey-footer__list">\n            <li class="survey-footer__list-item">\n              <span class="bold">\u0411\u044B\u0442\u044C \u043C\u043E\u0434\u043D\u044B\u043C \u0438 \u0441\u0442\u0438\u043B\u044C\u043D\u044B\u043C</span> \u0432\u0430\u0436\u043D\u043E \u0434\u043B\u044F \u043B\u044E\u0431\u043E\u0433\u043E \u0448\u043A\u043E\u043B\u044C\u043D\u0438\u043A\u0430. \u0421 \u043E\u0434\u0435\u0436\u0434\u043E\u0439 <a class="survey-link" href="https://www.next.com.ru/ru/back-to-school" target="_blank" onclick="gtag(\'event\', \'\u041E\u043F\u0440\u043E\u0441 \u0418\u043D\u0442\u0435\u0440\u043D\u0435\u0442 \u043C\u0430\u0433\u0430\u0437\u0438\u043D NEXT \u041A\u043B\u0438\u043A\', { \'event_category\': \'\u041E\u043F\u0440\u043E\u0441 \u0418\u043D\u0442\u0435\u0440\u043D\u0435\u0442 \u043C\u0430\u0433\u0430\u0437\u0438\u043D NEXT\', \'event_action\':\n              \'\u041A\u043B\u0438\u043A\', });">\u0438\u043D\u0442\u0435\u0440\u043D\u0435\u0442-\u043C\u0430\u0433\u0430\u0437\u0438\u043D\u0430 NEXT</a> \u0435\u0433\u043E \u0448\u043A\u043E\u043B\u044C\u043D\u044B\u0439 \u043E\u0431\u0440\u0430\u0437 \u0431\u0443\u0434\u0435\u0442 \u0431\u0435\u0437\u0443\u043F\u0440\u0435\u0447\u043D\u044B\u043C.\n            </li>\n\n            <li class="survey-footer__list-item">\n              \u0428\u043A\u043E\u043B\u044C\u043D\u0443\u044E \u0444\u043E\u0440\u043C\u0443 \u043E\u0442 <a class="survey-link" href="https://www.next.com.ru/ru/back-to-school" target="_blank" onclick="gtag(\'event\', \'\u041E\u043F\u0440\u043E\u0441 \u0418\u043D\u0442\u0435\u0440\u043D\u0435\u0442 \u043C\u0430\u0433\u0430\u0437\u0438\u043D NEXT \u041A\u043B\u0438\u043A\', { \'event_category\': \'\u041E\u043F\u0440\u043E\u0441 \u0418\u043D\u0442\u0435\u0440\u043D\u0435\u0442 \u043C\u0430\u0433\u0430\u0437\u0438\u043D NEXT\', \'event_action\':\n              \'\u041A\u043B\u0438\u043A\', });">NEXT</a> <span class="bold">\u043B\u0435\u0433\u043A\u043E \u0441\u0442\u0438\u0440\u0430\u0442\u044C \u0438 \u0433\u043B\u0430\u0434\u0438\u0442\u044C.</span> \u0427\u0442\u043E\u0431\u044B \u043F\u043E\u0434\u0433\u043E\u0442\u043E\u0432\u0438\u0442\u044C \u0440\u0435\u0431\u0435\u043D\u043A\u0430 \u043A \u0448\u043A\u043E\u043B\u0435, \u0432\u0430\u043C \u043F\u043E\u0442\u0440\u0435\u0431\u0443\u0435\u0442\u0441\u044F \u0441\u043E\u0432\u0441\u0435\u043C \u043D\u0435\u043C\u043D\u043E\u0433\u043E \u0432\u0440\u0435\u043C\u0435\u043D\u0438.\n            </li>\n\n            <li class="survey-footer__list-item">\n              \u041E\u0434\u0435\u0436\u0434\u0430 \u0438\u0437 <a class="survey-link" href="https://www.next.com.ru/ru/back-to-school" target="_blank" onclick="gtag(\'event\', \'\u041E\u043F\u0440\u043E\u0441 \u0418\u043D\u0442\u0435\u0440\u043D\u0435\u0442 \u043C\u0430\u0433\u0430\u0437\u0438\u043D NEXT \u041A\u043B\u0438\u043A\', { \'event_category\': \'\u041E\u043F\u0440\u043E\u0441 \u0418\u043D\u0442\u0435\u0440\u043D\u0435\u0442 \u043C\u0430\u0433\u0430\u0437\u0438\u043D NEXT\', \'event_action\':\n              \'\u041A\u043B\u0438\u043A\', });">\u0438\u043D\u0442\u0435\u0440\u043D\u0435\u0442-\u043C\u0430\u0433\u0430\u0437\u0438\u043D\u0430 NEXT</a> \u0438\u0437\u0433\u043E\u0442\u043E\u0432\u043B\u0435\u043D\u0430 <span class="bold">\u0438\u0437 \u043A\u0430\u0447\u0435\u0441\u0442\u0432\u0435\u043D\u043D\u044B\u0445 \u043C\u0430\u0442\u0435\u0440\u0438\u0430\u043B\u043E\u0432,</span> \u043A\u043E\u0442\u043E\u0440\u044B\u0435 \u0438\u0437\u043D\u0430\u0448\u0438\u0432\u0430\u044E\u0442\u0441\u044F \u043C\u0435\u0434\u043B\u0435\u043D\u043D\u043E. \u0428\u043A\u043E\u043B\u044C\u043D\u0430\u044F \u0444\u043E\u0440\u043C\u0430 \u043F\u0440\u043E\u0441\u043B\u0443\u0436\u0438\u0442 \u0432\u0430\u043C \u0434\u043E\u043B\u0433\u043E.\n            </li>\n          </ul>\n        </div>\n        <div class="survey-footer__col">\n          <iframe width="427" height="240" class="video-payer" src="https://www.youtube.com/embed/HWvND1ZFpco?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen onclick="gtag(\'event\', \'\u041E\u043F\u0440\u043E\u0441 \u0412\u0438\u0434\u0435\u043E \u041A\u043B\u0438\u043A\', { \'event_category\': \'\u041E\u043F\u0440\u043E\u0441 \u0412\u0438\u0434\u0435\u043E\', \'event_action\':\n              \'\u041A\u043B\u0438\u043A\', });"></iframe>\n\n          <div class="survey-footer__link-container">\n            <a class="res-btn link-btn" href="https://www.next.com.ru/ru/back-to-school" target="_blank" onclick="gtag(\'event\', \'\u041E\u043F\u0440\u043E\u0441 \u041F\u0435\u0440\u0435\u0439\u0442\u0438 \u043D\u0430 \u0441\u0430\u0439\u0442 \u041A\u043B\u0438\u043A\', { \'event_category\': \'\u041E\u043F\u0440\u043E\u0441 \u041F\u0435\u0440\u0435\u0439\u0442\u0438 \u043D\u0430 \u0441\u0430\u0439\u0442\', \'event_action\':\n              \'\u041A\u043B\u0438\u043A\', });">\u041F\u0415\u0420\u0415\u0419\u0422\u0418 \u041D\u0410 \u0421\u0410\u0419\u0422</a>\n            <ul class="survey-footer__link-list">\n              <li class="survey-footer__link-list-item">\n                <a class="survey-link survey-link--mob" href="https://www.next.com.ru/ru/back-to-school" target="_blank">\n                  \u041A\u0430\u0442\u0430\u043B\u043E\u0433 \u0441\u043E\u043F\u0443\u0442\u0441\u0442\u0432\u0443\u044E\u0449\u0438\u0445 \u0442\u043E\u0432\u0430\u0440\u043E\u0432\n                </a>\n              </li>\n\n              <li class="survey-footer__link-list-item">\n                <a class="survey-link" href="https://www.next.com.ru/ru/back-to-school" target="_blank">\n                  \u0410\u043A\u0446\u0438\u0438 \u0438 \u043F\u0440\u0435\u0434\u043B\u043E\u0436\u0435\u043D\u0438\u044F\n                </a>\n              </li>\n            </ul>\n          </div>\n        </div>\n      </div>\n    ';
      $('.survey-content').html(html);
      $('.survey-container').show();
      $('.survey-content').scrollTop(0);
    });
  }
};

App.init();

}());
//# sourceMappingURL=app.js.map

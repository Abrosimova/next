import Cookie from './modules/cookie.js'
import { API } from './modules/api.js'
import { Auth } from './modules/auth.js'
import appOptions from './modules/settings.js'
import content from './modules/content.js'
import { Loader } from './modules/loader.js'
import { Modal } from './modules/modal.js'
import Activity from './modules/activity.js'
import Result from './modules/result.js'

API.init();
if (document.location.href.indexOf('access_token') > -1) {
  Auth.auth(function () {
    var path = window.location.pathname.substring(0, window.location.pathname.length);

    history.pushState('', document.title, path);
    API.token = Cookie.get(appOptions.provider + '_token');
    API.getUserInfo(() => {
      checkSession();
    });
  });
} else if (typeof Cookie.get(appOptions.provider + '_token') !== 'undefined') {
  API.token = Cookie.get(appOptions.provider + '_token');
  API.getUserInfo(() => {
    checkSession();
  });
} else {
  let html = `
    <div class="auth-title">Авторизация</div>
    <div class="auth-text">Для продолжения необходимо авторизоваться</div>
    <button class="res-btn auth-btn">авторизация</button>
  `

  Modal.open(html)
  $('.auth-btn').on('click', function() {
    Auth.auth()
  })
}

function checkSession() {
  Loader.show();
  Activity.get(() => {
    Loader.hide();
    if (Activity.model.result.length) {
      // drawSurvey()
      drawResults()
    } else {
      drawSurvey()
    }
  })
}

function drawResults() {
  Loader.show()
  Result.get(() => {
    Loader.hide() 
    $('.survey-container').hide();
    let html = ''

    $('.survey-title__text').html('Вот что думают родители школьников о качестве школьной формы')
    $('.survey-title__subtitle').hide()
    for (let i in Result.model) {
      if (!Result.model.hasOwnProperty(i)) {
        continue
      }
      let total = 0;

      for (let k in Result.model[i]) {
        if (!Result.model[i].hasOwnProperty(k)) {
          continue
        }
        total += Result.model[i][k]
      }
      const item = content.result.find(item => item.id === parseInt(i, 10))

      html += `
        <div class="result-item">
          <div class="result-item__header">
            <img class="result-item__header-icon" src="${ item.icon }"/>
            <div class="result-item__header-text"> ${ item.text } </div>
          </div>
        `
      for (let j = 0; j < item.answers.length; j++) {
        let width = Result.model[i][item.answers[j].id] / total * 100

        html += `
          <div class="result-item__answer">
            <div class="result-item__answer-text"> ${ item.answers[j].text} </div>
            <div class="result-item__progress">`
        html += width > 0 ? `<div class="result-item__progress-line" style="width: ${ width }%">
          <div class="result-item__progress-text"> ${ parseInt(width, 10) }% </div>
        </div>` : ''
        html += `</div>
          </div>
        `
      }  
      html += '</div>'
    }
    html += `
      <div class="survey-footer">
        <div class="survey-footer__col">
          <div class="survey-footer__title">
            Почему NEXT?
          </div> 
          <ul class="survey-footer__list">
            <li class="survey-footer__list-item">
              <span class="bold">Быть модным и стильным</span> важно для любого школьника. С одеждой <a class="survey-link" href="https://www.next.com.ru/ru/back-to-school" target="_blank">интернет-магазина NEXT</a> его школьный образ будет безупречным.
            </li>

            <li class="survey-footer__list-item">
              Школьную форму от <a class="survey-link" href="https://www.next.com.ru/ru/back-to-school" target="_blank">NEXT</a> <span class="bold">легко стирать и гладить.</span> Чтобы подготовить ребенка к школе, вам потребуется совсем немного времени.
            </li>

            <li class="survey-footer__list-item">
              Одежда из <a class="survey-link" href="https://www.next.com.ru/ru/back-to-school" target="_blank">интернет-магазина NEXT</a> изготовлена <span class="bold">из качественных материалов,</span> которые изнашиваются медленно. Школьная форма прослужит вам долго.
            </li>
          </ul>
        </div>
        <div class="survey-footer__col">
          <iframe width="427" height="240" class="video-payer" src="https://www.youtube.com/embed/HWvND1ZFpco?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

          <div class="survey-footer__link-container">
            <a class="res-btn link-btn" href="https://www.next.com.ru/ru/back-to-school" target="_blank">ПЕРЕЙТИ НА САЙТ</a>
            <ul class="survey-footer__link-list">
              <li class="survey-footer__link-list-item">
                <a class="survey-link" href="https://www.next.com.ru/ru/back-to-school" target="_blank">
                  Каталог сопутствующих товаров
                </a>
              </li>

              <li class="survey-footer__link-list-item">
                <a class="survey-link" href="https://www.next.com.ru/ru/back-to-school" target="_blank">
                  Акции и предложения
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    `
    $('.survey-content').html(html)
    $('.survey-container').show();
  })
}

function drawSurvey() {
  $('.survey-title__text').html('Довольны ли вы качеством школьной формы?')
  $('.survey-title__subtitle').html('Оцените форму, которую носит ваш ребенок')
  let html = ''

  for (let i = 0; i < content.test.length; i++) {
    html += `
      <div class="survey-item" data-q="${ content.test[i].id }">
        <div class="survey-item__question">
          ${ content.test[i].id}. ${content.test[i].text }
        </div>
    `
    for (let j = 0; j < content.test[i].answers.length; j++) {
      html += `
        <div 
          class="survey-item__answer
          ${content.test[i].multi ? ' survey-item__answer--check' : ''}" 
          data-id="${content.test[i].answers[j].id}"
        >
          ${ content.test[i].answers[j].text }
        </div>
      `
    }    
    html += '</div>'
  }
  html += `
    <div class="error-container">
      Для продолжения необходимо ответить на все вопросы
    </div>
    <button class="res-btn send-survey">узнать результат</button>
  `
  $('.survey-content').html(html)
  $('.survey-container').show();

  $('.survey-item__answer').on('click', function() {
    const parent = $(this).parents('.survey-item')

    if ($(this).hasClass('survey-item__answer--check')) {
      $(this).toggleClass('survey-item__answer--active')
    } else {
      parent.children('.survey-item__answer').removeClass('survey-item__answer--active')
      $(this).addClass('survey-item__answer--active')
    }
    
  })

  $('.send-survey').on('click', function() {
    let res = []
    let error = false

    $('.survey-item').each(function() {
      let ans = []

      $(this).children('.survey-item__answer--active').each(function() {
        ans.push($(this).data('id'))
      })
      if (ans.length) {
        res.push({ id: $(this).data('q'), answer: ans }) 
      } else {
        error = true
      }
    })
    if (error) {
      $('.error-container').show();
    } else {
      $('.error-container').hide();
      Result.get(() => {
        for (let i = 0; i < res.length; i++) {
          for (let j = 0; j < res[i].answer.length; j++) {
            Result.model[res[i].id][res[i].answer[j]] += 1
          }
        } 
        Loader.show()
        Result.set(() => {
          Activity.model.result = res;
          Activity.set(() => {
            Loader.hide()
            drawResults()
          })
        })
      })
    }
  })
}
import Cookie from './modules/cookie.js'
import { API } from './modules/api.js'
import { Auth } from './modules/auth.js'
import appOptions from './modules/settings.js'
import content from './modules/content.js'
import { Loader } from './modules/loader.js'
import { Modal } from './modules/modal.js'
import Activity from './modules/activity.js'
import Result from './modules/result.js'
import { wordForm } from './modules/common.js'
import { dateTime } from './modules/common.js'

let App = {
  totalCount: 0,
  currentCount: 0,
  overflow: '',
  ext: '',
  imageWidth: 0,
  imageHeight: 0,
  base64: [],
  photos: [],

  init: function () {
    const that = this;

    API.init();

    $('.photo-miniature__img').on('click', function() {
      $('.photo-large__img').prop('src', $(this).prop('src'))
      $('.photo-large__link').prop('href', $(this).data('href')) 
    })

    $(window).on('resize scroll', function () {
      if (!appOptions.isMobile) {
        return
      }
      const scroll = Math.floor($(document).height() / $(window).scrollTop())

      $('.col').removeClass('col--active')
      if (scroll < 5) {
        $('.col--r').addClass('col--active')
      } else if (scroll < 20) {
        $('.col--c').addClass('col--active')
      } else {
        $('.col--l').addClass('col--active') 
      }
    });

    $('.contest-desc__road-map-item').hover(function() {
      const id = $(this).data('id')

      $('.contest-desc__road-map-item--start').removeClass('contest-desc__road-map-item--start')
      $('.contest-desc__msg-mob--start').removeClass('contest-desc__msg-mob--start')
      $(`.contest-desc__msg-mob[data-id=${id}]`).css('opacity', '1')
    }, function() {
      const id = $(this).data('id')

      $(`.contest-desc__msg-mob[data-id=${id}]`).css('opacity', '0')
    })

    $('.nav-mobile').on('click', function() {
      $('.survey-bg__item-text[data-id="1"]').click()
    })

    $('.next-btn, .col, .col__content').on('click', function (e) {
      // e.preventDefault();
      e.stopPropagation();
      let id = $(this).data('id')

      if ($(this).hasClass('col--large')) {
        return
      }

      switch (id) {
        case 1: 
          if ($(this).hasClass('next-btn') || appOptions.isMobile && $(this).hasClass('col__content')) {
            window.open('https://www.next.com.ru/ru/back-to-school')
          } else {
            $('.survey-bg__item-text').addClass('survey-bg__item-text--hide').removeClass('survey-bg__item-text--show');
            $('.col').removeClass('col--large col--page col--c-large col--r-large')
            $('.nav-mobile').removeClass('nav-mobile-show')
            $('.col__content').fadeIn()

            $('.header').show()
            $('.survey-container').hide();
            $('.contest-page').hide();
          }
          break;
        case 2: 
          $('.col__content').hide()
          $('.contest-page').hide();
          $('.header').hide()

          $('.nav-mobile').addClass('nav-mobile-show')

          $('.col').removeClass('col--large col--r-large').addClass('col--page')
          $('.col--c').addClass('col--large col--c-large')

          $('.col--l').children('.survey-bg__item-text').removeClass('survey-bg__item-text--hide').addClass('survey-bg__item-text--show');
          $('.col--r').children('.survey-bg__item-text').removeClass('survey-bg__item-text--hide').addClass('survey-bg__item-text--show');
          $('.col--c').children('.survey-bg__item-text').addClass('survey-bg__item-text--hide').addClass('survey-bg__item-text--show');

          $('.survey-container').fadeIn();
          that.checkSession();
          break;
        case 3: 
          $('.col__content').hide()
          $('.survey-container').hide();
          $('.header').hide()

          $('.nav-mobile').addClass('nav-mobile-show')

          $('.col').removeClass('col--large col--c-large').addClass('col--page')
          $('.col--r').addClass('col--large col--r-large')

          $('.col--l').children('.survey-bg__item-text').removeClass('survey-bg__item-text--hide').addClass('survey-bg__item-text--show');
          $('.col--c').children('.survey-bg__item-text').removeClass('survey-bg__item-text--hide').addClass('survey-bg__item-text--show');
          $('.col--r').children('.survey-bg__item-text').addClass('survey-bg__item-text--hide').addClass('survey-bg__item-text--show');

          $('.contest-page').fadeIn();
          that.checkSession();
          break;  
      }
    })

    $('.send-photo').on('click', function () {
      $('.loader-layout').css('display', 'flex')
      that.overflow = $('body').css('overflow');
      $('body').css('overflow', 'hidden');
    })

    $('.modal__close').on('click', function () {
      $(this).parents('.loader-layout').css('display', 'none');
      $('body').css('overflow', that.overflow);
    });

    $('.photo__placeholder').on('click', function () {
      $('.hidden-form')[0].reset();
      $('.photo-loader').click();
    });

    $('.photo-loader').on('change', function (e) {
      e.preventDefault();
      if (!e.target.files.length) {
        return;
      }
      Loader.show();
      const reader = new FileReader();

      const fileName = e.target.files[0].name.split('.');

      that.ext = fileName[fileName.length - 1];
      reader.onload = (event) => {
        const img = new Image();

        img.onload = function loader() {
          Loader.hide();
          const ratioX = 110 / this.width;
          const ratioY = 80 / this.height;
          const ratio = ratioX < ratioY ? ratioX : ratioY;

          that.imageWidth = parseInt(this.width * ratio, 10);
          that.imageHeight = parseInt(this.height * ratio, 10);
          that.base64.push(event.target.result);
          if (that.base64[that.base64.length - 1].length > 6000000) {
            $('.warn').html(`
          Изображение слишком большое.<br>
          Загрузите изображение меньшего размера в формате png, jpg или gif`);
            that.base64 = [];
          } else {
            let html = `
          <img src="${ that.base64[that.base64.length - 1]}" width="${that.imageWidth}" height="${that.imageHeight}" class="loaded-photo">
          <span class="remove-photo"></span>`

            $('.photo__placeholder').html(html);
          }
        };
        img.src = URL.createObjectURL(e.target.files[0]);
      };
      reader.readAsDataURL(e.target.files[0]);
    });

    $('.photo__placeholder').on('click', '.remove-photo', function (e) {
      e.stopPropagation();
      $(this).prev('.loaded-photo').remove();
      $(this).remove();
    })

    $('.load-photo').on('click', function () {
      if (that.base64.length === 0) {
        return;
      }
      $('.warn').empty();
      Loader.show();
      const files = [];

      that.base64.forEach(item => files.push({
        name: `${new Date().getTime()}.${that.ext}`,
        file: item.split('base64,')[1]
      }))
      that.ext = '';
      that.base64 = [];
      API.uploadImage(files, (res) => {
        if (res) {
          API.checkUploadImage(res, (uploadFiles) => {
            Activity.model.photo.push(...uploadFiles.map(item => item.downloadUrl));
            API.addKeyToDB({
              label: appOptions.provider + '-gallery',
              key: appOptions.provider + '-photo_' + API.model.user.id_str + '-' + new Date().getTime(),
              value: JSON.stringify({ user: API.model.user, photo: uploadFiles[0].downloadUrl })
            }, function () {
              Loader.hide();
            });
            Activity.set();
            if (uploadFiles) {
              $('.warn').html('Изображение успешно загружено');
              $('.photo__placeholder').empty();
              $('.modal__close').click();
              that.getGallery()
            } else {
              $('.warn').html('Произошла ошибка при загрузке изображения.');
            }
          });
        } else {
          Loader.hide();
          $('.warn').html('Произошла ошибка при загрузке изображения.');
        }
      });
    });
  },

  checkSession: function() {
    const that = this;

    if (document.location.href.indexOf('access_token') > -1) {
      Auth.auth(function () {
        var path = window.location.pathname.substring(0, window.location.pathname.length);

        history.pushState('', document.title, path);
        API.token = Cookie.get(appOptions.provider + '_token');
        check()
      });
    } else if (typeof Cookie.get(appOptions.provider + '_token') !== 'undefined') {
      API.token = Cookie.get(appOptions.provider + '_token');
      check()
    } else {
      let html = `
        <div class="auth-title">Авторизация</div>
        <div class="auth-text">Для продолжения необходимо авторизоваться</div>
        <button class="res-btn auth-btn">авторизация</button>
      `

      Modal.open(html)
      $('.auth-btn').on('click', function () {
        Auth.auth()
      })
    }

    function check() {
      Loader.show();
      API.getUserInfo(() => {
        Activity.get(() => {
          Loader.hide();
          if ($('.col--large').data('id') === 2) {
            if (Activity.model.result.length) {
              // that.drawSurvey()
              that.drawResults()
            } else {
              that.drawSurvey()
            }
          } else {
            that.getGallery()
          }
        })
      })
    }
  },

  getGallery: function(step) {
    const that = this;

    Loader.show()
    API.getKeysFromDB({
      label: appOptions.provider + '-gallery',
      pageSize: 50,
      pageNumber: step || 1,
      orderBy: 'date_desc'
    }, function (data) {
      Loader.hide() 
      that.drawGallery(data, step)
    })
  },

  drawGallery: function(data, step) {
    const that = this;
    const newData = data.Keys.map(item => {
      return {
        key: item.Key,
        date: item.CreatedDate,
        data: JSON.parse(item.Value)
      }
    })
    let html = ''

    that.photos = [...that.photos, ...newData]

    if (typeof step === 'undefined') {
      this.totalCount = data.Paging.count;
      html += `
    <div class="gallery-container">
      <div class="gallery__title">
        Уже <span class="count">${ this.totalCount}</span> ${wordForm.get('participant', this.totalCount)}
      </div>
      <div class="gallery-content">
    `
    }
    this.currentCount = that.photos.length

    for (let i = (step - 1 || 0) * 50; i < that.photos.length; i++) {
      html += `
      <div class="gallery__item" data-id="${that.photos[i].key}">
        ${ appOptions.admins.includes(API.model.user.id_str) ?
          `<div class="gallery__item-remove" data-id="${that.photos[i].key}">x</div>` : ''}
        
        <img class="gallery__item-photo" src="${ that.photos[i].data.photo}">
      </div>
    `
    }

    if (typeof step === 'undefined') {
      html += '</div>'
      if (this.totalCount > this.currentCount) {
        html += '<button class="res-btn next-step" data-id="2">Загрузить еще</button>'
      }
      html += '</div>'
    } else if (this.totalCount > this.currentCount) {
      html += `<button class="res-btn next-step" data-id="${step + 1}">Загрузить еще</button>`
    }

    if (typeof step === 'undefined') {
      $('.gallery-container').remove()
      $('.contest-page').append(html)
    } else {
      $('.gallery-content').append(html)
    }

    $('.gallery__item').on('click', function () {
      const img = new Image();
      const data = that.photos.find(item => item.key === $(this).data('id'))

      img.onload = function loader() {
        let imgW = this.width;
        let imgH = this.height;
        let ratio = this.width / this.height;

        ratio = ratio > 1 ? ratio : this.height / this.width
        if (this.width >= this.height) {
          imgW = this.width > document.documentElement.clientWidth * 0.7 ?
            document.documentElement.clientWidth * 0.7 : this.width;
          imgH = imgW / ratio;
          if (imgH > document.body.clientHeight * 0.7) {
            imgH = document.body.clientHeight * 0.7;
            imgW = imgH * ratio;
          }
        } else {
          imgH = this.height > document.body.clientHeight * 0.7 ?
            document.body.clientHeight * 0.7 : this.height;
          imgW = imgH / ratio;
        }
        const mLeft = parseInt(imgW / 2, 10);
        const mTop = parseInt(imgH / 2, 10);
        const closeLeft = (imgW / 2) + 20;

        let html = `
        <div class="view-layout">
          <div class="close-viewer" style="margin: -${ mTop}px 0 0 ${closeLeft}px"></div>
          <div class="user-name" style="margin: ${mTop + 10}px 0 0 -${mLeft}px" >
            <a href="${ appOptions.userLink}${data.data.user.id_str}" class="user-name__link" target="_blank">
              ${ data.data.user.firstName} ${data.data.user.lastName }
            </a>
            <div class="user-name__date">Дата добавления: ${dateTime.get(data.date)}</div>
          </div>
          <img src="${ this.src}" alt="" style="margin: -${mTop}px 0 0 -${mLeft}px" width="${imgW}" height="${imgH}" class="view-photo">
        </div>
      `

        $('body').append(html);
        that.overflow = $('body').css('overflow');
        $('body').css('overflow', 'hidden');

        $('.close-viewer').on('click', function () {
          $('.view-layout').remove();
          $('body').css('overflow', that.overflow);
        })
      };
      img.src = $(this).children('.gallery__item-photo').prop('src');
    })

    $('.gallery__item-remove').on('click', function (e) {
      e.stopPropagation()
      Loader.show();
      API.deleteKeyFromDB($(this).data('id'), function () {
        Loader.hide();
        that.getGallery();
      });
    });

    $('.contest-page').on('click', '.next-step', function () {
      that.getGallery($(this).data('id'))
      $(this).remove()
    })
  },

  drawSurvey: function() {
    const that = this;

    $('.survey-title__text').html('Довольны ли вы качеством школьной формы?')
    $('.survey-title__subtitle').html('Оцените форму, которую носит ваш ребенок')
    let html = ''

    for (let i = 0; i < content.test.length; i++) {
      html += `
      <div class="survey-item" data-q="${ content.test[i].id}">
        <div class="survey-item__question">
          ${ content.test[i].id}. ${content.test[i].text}
        </div>
    `
      for (let j = 0; j < content.test[i].answers.length; j++) {
        html += `
        <div 
          class="survey-item__answer
          ${content.test[i].multi ? ' survey-item__answer--check' : ''}" 
          data-id="${content.test[i].answers[j].id}"
        >
          ${ content.test[i].answers[j].text}
        </div>
      `
      }
      html += '</div>'
    }
    html += `
    <div class="error-container">
      Для продолжения необходимо ответить на все вопросы
    </div>
    <button class="res-btn send-survey" onclick="gtag('event', 'Опрос Узнать результат Клик', { 'event_category': 'Опрос Узнать результат', 'event_action':
              'Клик', });">узнать результат</button>
  `
    $('.survey-content').html(html)
    $('.survey-container').show();

    $('.survey-item__answer').on('click', function () {
      const parent = $(this).parents('.survey-item')

      if ($(this).hasClass('survey-item__answer--check')) {
        $(this).toggleClass('survey-item__answer--active')
      } else {
        parent.children('.survey-item__answer').removeClass('survey-item__answer--active')
        $(this).addClass('survey-item__answer--active')
      }

    })

    $('.send-survey').on('click', function () {
      let res = []
      let error = false

      $('.survey-item').each(function () {
        let ans = []

        $(this).children('.survey-item__answer--active').each(function () {
          ans.push($(this).data('id'))
        })
        if (ans.length) {
          res.push({ id: $(this).data('q'), answer: ans })
        } else {
          error = true
        }
      })
      if (error) {
        $('.error-container').show();
      } else {
        $('.error-container').hide();
        Result.get(() => {
          for (let i = 0; i < res.length; i++) {
            for (let j = 0; j < res[i].answer.length; j++) {
              Result.model[res[i].id][res[i].answer[j]] += 1
            }
          }
          Loader.show()
          Result.set(() => {
            Activity.model.result = res;
            Activity.set(() => {
              Loader.hide()
              that.drawResults()
            })
          })
        })
      }
    })
  },

  drawResults: function() {
    Loader.show()
    Result.get(() => {
      Loader.hide()
      $('.survey-container').hide();
      let html = ''

      $('.survey-title__text').html('Вот что думают родители школьников о качестве школьной формы')
      $('.survey-title__subtitle').hide()
      for (let i in Result.model) {
        if (!Result.model.hasOwnProperty(i)) {
          continue
        }
        let total = 0;

        for (let k in Result.model[i]) {
          if (!Result.model[i].hasOwnProperty(k)) {
            continue
          }
          total += Result.model[i][k]
        }
        const item = content.result.find(item => item.id === parseInt(i, 10))

        html += `
        <div class="result-item">
          <div class="result-item__header">
            <img class="result-item__header-icon" src="${ item.icon}"/>
            <div class="result-item__header-text"> ${ item.text} </div>
          </div>
        `
        for (let j = 0; j < item.answers.length; j++) {
          let width = Result.model[i][item.answers[j].id] / total * 100

          html += `
          <div class="result-item__answer">
            <div class="result-item__answer-text"> ${ item.answers[j].text} </div>
            <div class="result-item__progress">`
          html += width > 0 ? `<div class="result-item__progress-line" style="width: ${width}%">
          <div class="result-item__progress-text"> ${ parseInt(width, 10)}% </div>
        </div>` : ''
          html += `</div>
          </div>
        `
        }
        html += '</div>'
      }
      html += `
      <div class="survey-footer">
        <div class="survey-footer__col">
          <div class="survey-footer__title">
            Почему NEXT?
          </div> 
          <ul class="survey-footer__list">
            <li class="survey-footer__list-item">
              <span class="bold">Быть модным и стильным</span> важно для любого школьника. С одеждой <a class="survey-link" href="https://www.next.com.ru/ru/back-to-school" target="_blank" onclick="gtag('event', 'Опрос Интернет магазин NEXT Клик', { 'event_category': 'Опрос Интернет магазин NEXT', 'event_action':
              'Клик', });">интернет-магазина NEXT</a> его школьный образ будет безупречным.
            </li>

            <li class="survey-footer__list-item">
              Школьную форму от <a class="survey-link" href="https://www.next.com.ru/ru/back-to-school" target="_blank" onclick="gtag('event', 'Опрос Интернет магазин NEXT Клик', { 'event_category': 'Опрос Интернет магазин NEXT', 'event_action':
              'Клик', });">NEXT</a> <span class="bold">легко стирать и гладить.</span> Чтобы подготовить ребенка к школе, вам потребуется совсем немного времени.
            </li>

            <li class="survey-footer__list-item">
              Одежда из <a class="survey-link" href="https://www.next.com.ru/ru/back-to-school" target="_blank" onclick="gtag('event', 'Опрос Интернет магазин NEXT Клик', { 'event_category': 'Опрос Интернет магазин NEXT', 'event_action':
              'Клик', });">интернет-магазина NEXT</a> изготовлена <span class="bold">из качественных материалов,</span> которые изнашиваются медленно. Школьная форма прослужит вам долго.
            </li>
          </ul>
        </div>
        <div class="survey-footer__col">
          <iframe width="427" height="240" class="video-payer" src="https://www.youtube.com/embed/HWvND1ZFpco?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen onclick="gtag('event', 'Опрос Видео Клик', { 'event_category': 'Опрос Видео', 'event_action':
              'Клик', });"></iframe>

          <div class="survey-footer__link-container">
            <a class="res-btn link-btn" href="https://www.next.com.ru/ru/back-to-school" target="_blank" onclick="gtag('event', 'Опрос Перейти на сайт Клик', { 'event_category': 'Опрос Перейти на сайт', 'event_action':
              'Клик', });">ПЕРЕЙТИ НА САЙТ</a>
            <ul class="survey-footer__link-list">
              <li class="survey-footer__link-list-item">
                <a class="survey-link survey-link--mob" href="https://www.next.com.ru/ru/back-to-school" target="_blank">
                  Каталог сопутствующих товаров
                </a>
              </li>

              <li class="survey-footer__link-list-item">
                <a class="survey-link" href="https://www.next.com.ru/ru/back-to-school" target="_blank">
                  Акции и предложения
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    `
      $('.survey-content').html(html)
      $('.survey-container').show();
      $('.survey-content').scrollTop(0)
    })
  }
}

App.init()
import Cookie from './cookie.js'
import { API } from './api.js'
import { Auth } from './auth.js'
import appOptions from './settings.js'
// import { Loader } from './modules/loader.js'
import { Modal } from './modal.js'
// import { wordForm } from './modules/common.js'
import Activity from './activity-pers.js'

export default {
  init() {
    const that = this;
    
    API.init();
    if (document.location.href.indexOf('access_token') > -1) {
      Auth.auth(function() {
        API.token = Auth.token;
        API.getUserInfo();
        var path = window.location.pathname.substring(0, window.location.pathname.length);

        history.pushState('', document.title, path);
        API.token = Cookie.get(appOptions.provider + '_token');
        that.getActivity();
      });
    } else if (typeof Cookie.get(appOptions.provider + '_token') !== 'undefined') {
      API.token = Cookie.get(appOptions.provider + '_token');
      this.getActivity();
    } else {
      Auth.auth();
    }
  },
  
  getActivity() {
    API.token = Cookie.get(appOptions.provider + '_token');
    API.getUserInfo(() => {
      Activity.get(() => {
        if (Activity.model.persId === -1) {
          this.drawPersModal();
        } else {
          this.drawPersHeader();
        }
      })
    }) 
  },
  
  drawPersModal() {
    let html = '';
    const that = this;

    html += '<img class="modal-logo" src="' + appOptions.cdnPath + 'game/logo.png" width="176" height="116">'
    html += '<div class="pers__title">выбери своего героя</div>'
    html += '<div class="pers__subtitle">С кем ты хочешь подружиться? Выбери героя, который будет помогать тебе учиться, узнавать новое и соблюдать питьевой режим!</div>'
    html += '<div class="pers-container">'
    html += '<img class="pers" data-id="2" src="' + appOptions.cdnPath + 'game/pers1.png" width="260" height="271">'
    html += '<img class="pers" data-id="3" src="' + appOptions.cdnPath + 'game/pers2.png" width="349" height="258">'
    html += '<img class="pers" data-id="4" src="' + appOptions.cdnPath + 'game/pers3.png" width="233" height="334">'
    html += '<img class="pers" data-id="1" src="' + appOptions.cdnPath + 'game/pers4.png" width="343" height="257">'
    html += '</div>'
    Modal.open(html, 0, 1);
    
    $('.pers').on('click', function() {
      Activity.model.persId = $(this).data('id')|0;
      Activity.set();
      let cookiePers;
      
      switch (Activity.model.persId) {
        case 1: 
          cookiePers = 1;
          break;
        case 2:
          cookiePers = 3;
          break;
        case 3:
          cookiePers = 2;
          break;
        case 4:
          cookiePers = 4;
          break;
      }
      const cookieItem = { chosenPers: cookiePers };

      Cookie.set('sportikDnevnik', JSON.stringify(cookieItem), { domain: appOptions.origin, path: '/' });
      that.drawPersHeader();
      Modal.close();
    });
    
    $('.modal-close').on('click', function() {
      that.drawPersHeader();
      Modal.close();
    });
  },
  
  drawPersHeader() {
    let html = `
      <img class="pers__header-img" src="${appOptions.cdnPath}p${Activity.model.persId}.png">
    `;
    const that = this;
    
    $('.pers__header-img').remove();
    $('.menu').after(html);
    
    $('.pers__header-img').on('click', function() {
      that.drawPersModal();
    });
  },
}
var isMobile = (function() {
  if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i)
  || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)
  || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i)
  || navigator.userAgent.match(/Windows Phone/i)) {
    return true;
  }

  return false;
})();

var locationUrl = document.location.href,
  appOptions;

if (locationUrl.indexOf('school.mosreg') > -1) {
  appOptions = {
    authUrl: 'https://login.school.mosreg.ru/oauth2',
    grantUrl: 'https://api.school.mosreg.ru/v1/authorizations',
    scope: 'CommonInfo,Files',
    clientId: '',
    redirectUrl: window.location.href + '/?auth=true',
    provider: 'next-survey',
    api: 'https://api.school.mosreg.ru/v1/',
    isMobile: isMobile,
    userLink: 'https://school.mosreg.ru/user/user.aspx?user=',
    cdnPath: 'https://ad.csdnevnik.ru/special/staging/next/img/',
    origin: '.school.mosreg.ru',
    admins: ['1000005762061', '1000005813233', '1000005052381']
  } 
} else {
  appOptions = {
    authUrl: 'https://login.dnevnik.ru/oauth2',
    grantUrl: 'https://api.dnevnik.ru/v1/authorizations',
    scope: 'CommonInfo,Files',
    clientId: 'd0287339476d4a32b0a2bbe67ca493fb',
    redirectUrl: window.location.href + '?auth=true',
    provider: 'next-survey',
    api: 'https://api.dnevnik.ru/v1/',
    apiMobile: 'https://api.dnevnik.ru/modile/v1/',
    isMobile: isMobile,
    userLink: 'https://dnevnik.ru/user/user.aspx?user=',
    cdnPath: 'https://ad.csdnevnik.ru/special/staging/next/img/',
    origin: '.dnevnik.ru',
    admins: ['1000006315838', '1000006435101', '1000004681017', '1000004934922']
  }
}

export default appOptions;

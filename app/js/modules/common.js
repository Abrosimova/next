var wordForm = {
  get: function(key, number) {
    var res, tail;

    tail = number;
    while (tail > 20) {
      tail -= 20;
    }
    switch (key) {
      case 'score':
        if (number == 1 || tail === 1 || tail === 11) {
          res = 'балл';
        } else if ((number >= 5 && number <= 20) || (tail >= 5 && tail <= 20) || tail === 0) {
          res = 'баллов';
        } else if (tail == 2 || tail == 3 || tail == 4) {
          res = 'балла';
        }
        break;
      case 'day':
        if (number == 1 || tail === 1 || tail === 11) {
          res = 'день';
        } else if ((number >= 5 && number <= 20) || (tail >= 5 && tail <= 20) || tail === 0) {
          res = 'дней';
        } else if (tail == 2 || tail == 3 || tail == 4) {
          res = 'дня';
        }
        break;
      case 'friend':
        if (number == 1 || tail === 1 || tail === 11) {
          res = 'друга';
        } else if ((number >= 5 && number <= 20) || (tail >= 5 && tail <= 20) || tail === 0) {
          res = 'друзей';
        } else if (tail == 2 || tail == 3 || tail == 4) {
          res = 'друга';
        }
        break;
      case 'second':
        if (number == 1 || tail === 1 || tail === 11) {
          res = 'секунда';
        } else if ((number >= 5 && number <= 20) || (tail >= 5 && tail <= 20) || tail === 0) {
          res = 'секунд';
        } else if (tail == 2 || tail == 3 || tail == 4) {
          res = 'секунды';
        }
        break;
      case 'lesson':
        if (number == 1 || tail === 1 || tail === 11) {
          res = 'урок';
        } else if ((number >= 5 && number <= 20) || (tail >= 5 && tail <= 20) || tail === 0) {
          res = 'уроков';
        } else if (tail == 2 || tail == 3 || tail == 4) {
          res = 'урока';
        }
        break;
      case 'hour':
        if (number == 1 || tail === 1 || tail === 11) {
          res = 'час';
        } else if ((number >= 5 && number <= 20) || (tail >= 5 && tail <= 20) || tail === 0) {
          res = 'часов';
        } else if (tail == 2 || tail == 3 || tail == 4) {
          res = 'часа';
        }
        break;
      case 'age':
        if (number == 1 || tail === 1) {
          res = 'год';
        } else if ((number >= 5 && number <= 20) || (tail >= 5 && tail <= 20) || tail === 0) {
          res = 'лет';
        }
        if (tail == 2 || tail == 3 || tail == 4 || number === 33) {
          res = 'года';
        }
        break;
      case 'participant':
        if (number == 1 || tail === 1) {
          res = 'участник';
        } else if ((number >= 5 && number <= 20) || (tail >= 5 && tail <= 20) || tail === 0) {
          res = 'участников';
        }
        if (tail == 2 || tail == 3 || tail == 4 || number === 33) {
          res = 'участника';
        }
        break;
    }

    return res;
  }
};

var dateTime = {
  get: function(time) {
    var d = new Date(time);
    var month = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
    var today = new Date();

    today.setHours(0);
    if (d.getTime() > today && d.getTime() < today + 24 * 60 * 60 * 1000) {
      return 'Сегодня'
    }
    var day = (d.getDate() < 10) ? '0' + d.getDate() : d.getDate();

    return day + '.' + month[d.getMonth()] + '.' + d.getFullYear();
  }
}

var shuffleArray = function(arr) {
  return arr.sort(function() {
    return Math.random() - 0.5
  })
}

export { wordForm, dateTime, shuffleArray }

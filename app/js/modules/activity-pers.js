import appOptions from './settings.js'
import { API } from './api.js'

export default {
  model: {},

  get(callback) {
    var that = this;

    API.getKeyFromDB({ key: appOptions.provider + '-activity-user_' + API.model.user.id_str }, function(data) {
//      data = null;
      if (data) {
        var json = JSON.parse(data.Value);

        that.model = json;
      } else {
        that.model = {
          persId: -1,
          user: API.model.user
        };
        that.set();
      }
      if (typeof callback === 'function') {
        callback(data === null);
      }
    });
  },

  set(callback) {
    var that = this;

    API.addKeyToDB({
      label: appOptions.provider + '-activity',
      key: appOptions.provider + '-activity-user_' + API.model.user.id_str,
      value: JSON.stringify(that.model)
    }, function() {
      if (typeof callback === 'function') {
        callback();
      }
    });
  }
}

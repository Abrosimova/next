import appOptions from './settings.js'
import { API } from './api.js'

export default {
  model: {},

  get(callback) {
    var that = this;

    API.getKeyFromDB({ key: appOptions.provider + '-survey-result' }, function (data) {
      // eslint-disable-next-line
      // data = null;
      if (data) {
        var json = JSON.parse(data.Value);

        that.model = json;
      } else {
        that.model = {
          1: {
            1: 0,
            2: 0,
            3: 0
          },
          2: {
            1: 0,
            2: 0,
            3: 0,
            4: 0
          },
          3: {
            1: 0,
            2: 0,
            3: 0
          },
          4: {
            1: 0,
            2: 0,
            3: 0,
            4: 0
          },
          5: {
            1: 0,
            2: 0,
            3: 0,
            4: 0,
            5: 0
          },
          6: {
            1: 0,
            2: 0
          },
          7: {
            1: 0,
            2: 0,
            3: 0
          },
          8: {
            1: 0,
            2: 0
          },
          9: {
            1: 0,
            2: 0,
            3: 0,
            4: 0,
            5: 0,
            6: 0
          }
        };
        that.set();
      }
      if (typeof callback === 'function') {
        callback(data === null);
      }
    });
  },

  set(callback) {
    var that = this;

    API.addKeyToDB({
      label: appOptions.provider + '-survey-result',
      key: appOptions.provider + '-survey-result',
      value: JSON.stringify(that.model)
    }, function () {
      if (typeof callback === 'function') {
        callback();
      }
    });
  }
}

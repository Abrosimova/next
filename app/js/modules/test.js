import Content from './content.js'

var Test = {
  init: function(callback) {
    this.questionCounter = -1;
    this.score = 0;
    this.model = Content.test;
    this.callback = callback;
    this.start();
  },

  start: function () {
    const that = this;

    // $('.test-button').on('click', function () {
    //   if (!$(this).hasClass('test-button--active')) {
    //     return
    //   }
    //   that.drawNextQuestion();
    // });
    $('.answers-container').after('<div class="answers-container"></div>').remove();
    $('.answers-container').on('click', '.answer-btn_2', function() {
      if ($('.answers-container--active').length === 0) {
        that.check($(this).data('i'));
        $('.answers-container').addClass('answers-container--active');
      }
    });

    $('.next-answer').off('click');
    $('.next-answer').on('click', function() {
      if ($('.answers-container').addClass('answers-container--active')) {
        that.drawNextQuestion();
      }
    });

    this.drawNextQuestion();
  },

  drawNextQuestion: function() {
    let html = '';
    // const that = this;

    this.questionCounter += 1;
    if (this.questionCounter === this.model.length) {
      if (typeof this.callback === 'function') {
        this.callback(Content.getResult(this.score), this.score);

        return;
      }
    }

    // $('.test-bullet').each(function() {
    //   if ($(this).data('id') === that.questionCounter + 1) {
    //     $(this).addClass('test-bullet--active');
    //   } else {
    //     $(this).removeClass('test-bullet--active');
    //   }
    // });
    $('.q-text__img').css('display', 'none');
    if (this.model[this.questionCounter].img) {
      $('.q-text__img').prop('src', this.model[this.questionCounter].img);
      $('.q-text__img').css('display', 'block');
    }
  
    $('.q-counter').html(`${this.questionCounter + 1} вопрос`);
    $('.q-text').html(this.model[this.questionCounter].text);
    for (let i = 0; i < this.model[this.questionCounter].answers.length; i ++) {
      html += `
        <button class="answer-btn_2" data-i="${i}">${this.model[this.questionCounter].answers[i].text}</button>
      `;
    }
    
    // $('.test-answer__text').remove();
    $('.answers-container').html(html);
    $('.answers-container').removeClass('answers-container--active');
  },

  check: function (answer) {
    const res = this.model[this.questionCounter].correct === answer;

    this.score += res ? 1 : 0;
    if (res) {
      $(`.answer-btn_2[data-i="${answer}"]`).addClass('answer-btn_2--correct');
    } else {
      $(`.answer-btn_2[data-i="${answer}"]`).addClass('answer-btn_2--incorrect');
    }
    $('.test-button').addClass('test-button--active');
  },

  getScore: function() {
    var index = 1,
      max = this.score[1];

    for (var i in this.score) {
      if (!this.score.hasOwnProperty(i)) {
        continue;
      }
      if (max < this.score[i]) {
        max = this.score[i];
        index = i;
      }
    }

    return index;
  },

  getQuestionById: function(id) {
    var data = this.model;

    for (var i = 0; i < data.length; i++) {
      if (data[i].id == id) {
        return data[i]
      }
    }

    return {};
  }
}

export { Test }

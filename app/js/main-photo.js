import Cookie from './modules/cookie.js'
import { API } from './modules/api.js'
import { Auth } from './modules/auth.js'
import appOptions from './modules/settings.js'
// import content from './modules/content.js'
import { Loader } from './modules/loader.js'
import { Modal } from './modules/modal.js'
import Activity from './modules/activity.js'
// import Result from './modules/result.js'
import { wordForm } from './modules/common.js'

API.init();
if (document.location.href.indexOf('access_token') > -1) {
  Auth.auth(function () {
    var path = window.location.pathname.substring(0, window.location.pathname.length);

    history.pushState('', document.title, path);
    API.token = Cookie.get(appOptions.provider + '_token');
    API.getUserInfo(() => {
      checkSession();
    });
  });
} else if (typeof Cookie.get(appOptions.provider + '_token') !== 'undefined') {
  API.token = Cookie.get(appOptions.provider + '_token');
  API.getUserInfo(() => {
    checkSession();
  });
} else {
  let html = `
    <div class="auth-title">Авторизация</div>
    <div class="auth-text">Для продолжения необходимо авторизоваться</div>
    <button class="res-btn auth-btn">авторизация</button>
  `

  Modal.open(html)
  $('.auth-btn').on('click', function () {
    Auth.auth()
  })
}

function checkSession() {
  Loader.show();
  Activity.get(() => {
    Loader.hide();
    getGallery()
  })
}

function getGallery(step) {
  Loader.show()
  API.getKeysFromDB({
    label: appOptions.provider + '-gallery',
    pageSize: 50,
    pageNumber: step || 1
  }, function (data) {
    // eslint-disable-next-line
    console.log(data)
    Loader.hide()
    drawGallery(data, step)
  })
}

function drawGallery(data, step) {
  let html = ''
  let photos = data.Keys.map(item => {
    return {
      key: item.Key,
      data: JSON.parse(item.Value)
    }
  })

  if (typeof step === 'undefined') {
    totalCount = data.Paging.count;
    html += `
    <div class="gallery-container">
      <div class="gallery__title">
        Уже <span class="count">${ totalCount}</span> ${wordForm.get('participant', totalCount) }
      </div>
      <div class="gallery-content">
    `
  }
  currentCount += photos.length
  
  for (let i = 0; i < photos.length; i++) {
    html += `
      <div class="gallery__item">
        ${ appOptions.admins.includes(API.model.user.id_str) ? 
          `<div class="gallery__item-remove" data-id="${photos[i].key }">x</div>` : '' }
        
        <img class="gallery__item-photo" src="${ photos[i].data.photo }">
      </div>
    `
  }

  if (typeof step === 'undefined') {
    html += '</div>'
    if (totalCount > currentCount) {
      html += '<button class="res-btn next-step" data-id="2">Загрузить еще</button>'
    }
    html += '</div>'
  } else if (totalCount > currentCount) {
    html += `<button class="res-btn next-step" data-id="${step + 1}">Загрузить еще</button>`
  }

  if (typeof step === 'undefined') {
    $('.gallery-container').remove()
    $('.contest-page').append(html)
  } else {
    $('.gallery-content').append(html)
  }
  
  $('.gallery__item').on('click', function () {
    const img = new Image();

    img.onload = function loader() {
      let imgW = this.width;
      let imgH = this.height;
      let ratio = this.width / this.height;

      ratio = ratio > 1 ? ratio : this.height / this.width
      if (this.width >= this.height) {
        imgW = this.width > document.documentElement.clientWidth * 0.7 ?
          document.documentElement.clientWidth * 0.7 : this.width;
        imgH = imgW / ratio;
        if (imgH > document.body.clientHeight * 0.7) {
          imgH = document.body.clientHeight * 0.7;
          imgW = imgH * ratio;
        }
      } else {
        imgH = this.height > document.body.clientHeight * 0.7 ?
          document.body.clientHeight * 0.7 : this.height;
        imgW = imgH / ratio;
      }
      const mLeft = parseInt(imgW / 2, 10);
      const mTop = parseInt(imgH / 2, 10);
      const closeLeft = (imgW / 2) + 20;

      let html = `
        <div class="view-layout">
          <div class="close-viewer" style="margin: -${ mTop}px 0 0 ${closeLeft}px"></div>
          <img src="${ this.src}" alt="" style="margin: -${mTop}px 0 0 -${mLeft}px" width="${imgW}" height="${imgH}" class="view-photo">
        </div>
      `

      $('body').append(html);
      overflow = $('body').css('overflow');
      $('body').css('overflow', 'hidden');

      $('.close-viewer').on('click', function () {
        $('.view-layout').remove();
        $('body').css('overflow', overflow);
      })
    };
    img.src = $(this).children('.gallery__item-photo').prop('src');
  })

  $('.gallery__item-remove').on('click', function (e) {
    e.stopPropagation()
    Loader.show();
    API.deleteKeyFromDB($(this).data('id'), function () {
      Loader.hide();
      getGallery();
    });
  });

  $('.contest-page').on('click', '.next-step', function() {
    getGallery($(this).data('id'))
    $(this).remove()
  })
}

let overflow;

$('.send-photo').on('click', function() {
  $('.loader-layout').css('display', 'flex')
  overflow = $('body').css('overflow');
  $('body').css('overflow', 'hidden');
})

$('.modal__close').on('click', function () {
  $(this).parents('.loader-layout').css('display', 'none');
  $('body').css('overflow', overflow);
});

let ext;
let imageWidth;
let imageHeight;
let base64 = [];
let totalCount = 0;
let currentCount = 0;

$('.photo__placeholder').on('click', function () {
  $('.hidden-form')[0].reset();
  $('.photo-loader').click();
});

$('.photo-loader').on('change', function (e) {
  e.preventDefault();
  if (!e.target.files.length) {
    return;
  }
  Loader.show();
  const reader = new FileReader();

  const fileName = e.target.files[0].name.split('.');

  ext = fileName[fileName.length - 1];
  reader.onload = (event) => {
    const img = new Image();

    img.onload = function loader() {
      Loader.hide();
      const ratioX = 110 / this.width;
      const ratioY = 80 / this.height;
      const ratio = ratioX < ratioY ? ratioX : ratioY;

      imageWidth = parseInt(this.width * ratio, 10);
      imageHeight = parseInt(this.height * ratio, 10);
      base64.push(event.target.result);
      if (base64[base64.length - 1].length > 6000000) {
        $('.warn').html(`
          Изображение слишком большое.<br>
          Загрузите изображение меньшего размера в формате png, jpg или gif`);
        base64 = [];
      } else {
        let html = `
          <img src="${ base64[base64.length - 1]}" width="${imageWidth}" height="${imageHeight}" class="loaded-photo">
          <span class="remove-photo"></span>`

        $('.photo__placeholder').html(html);
      }
    };
    img.src = URL.createObjectURL(e.target.files[0]);
  };
  reader.readAsDataURL(e.target.files[0]);
});

$('.photo__placeholder').on('click', '.remove-photo', function (e) {
  e.stopPropagation();
  $(this).prev('.loaded-photo').remove();
  $(this).remove();
})

$('.load-photo').on('click', function () {
  if (base64.length === 0) {
    return;
  }
  $('.warn').empty();
  Loader.show();
  const files = [];

  base64.forEach(item => files.push({
    name: `${new Date().getTime()}.${ext}`,
    file: item.split('base64,')[1]
  }))
  ext = '';
  base64 = [];
  API.uploadImage(files, (res) => {
    if (res) {
      API.checkUploadImage(res, (uploadFiles) => {
        Activity.model.photo.push(...uploadFiles.map(item => item.downloadUrl));
        API.addKeyToDB({
          label: appOptions.provider + '-gallery',
          key: appOptions.provider + '-photo_' + API.model.user.id_str + '-' + new Date().getTime(),
          value: JSON.stringify({ user: API.model.user, photo: uploadFiles[0].downloadUrl })
        }, function () {
          Loader.hide();
        });
        Activity.set();
        if (uploadFiles) {
          $('.warn').html('Изображение успешно загружено');
          $('.photo__placeholder').empty();
          $('.modal__close').click();
          getGallery()
        } else {
          $('.warn').html('Произошла ошибка при загрузке изображения.');
        }
      });
    } else {
      Loader.hide();
      $('.warn').html('Произошла ошибка при загрузке изображения.');
    }
  });
});
